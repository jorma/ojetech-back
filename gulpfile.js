const gulp = require('gulp');
const ts = require('gulp-typescript');
const apidoc = require('gulp-apidoc');
const sass = require('gulp-sass');
const rimraf = require('rimraf');
const tsProject = ts.createProject('tsconfig.json');

gulp.task('scripts', function() {
  const tsResult = tsProject.src().pipe(tsProject());

  // rimraf.sync('./dist');

  gulp.src(['src/views/**/*']).pipe(gulp.dest('dist/views'));
  gulp.src(['src/config/config.json']).pipe(gulp.dest('dist/config'));
  gulp.src(['src/config/configVars.json']).pipe(gulp.dest('dist/config'));
  gulp.src(['src/config/routes.json']).pipe(gulp.dest('dist/config'));

  return tsResult.js.pipe(gulp.dest('dist'));
});

gulp.task('apidoc', function(done) {
  apidoc({
    debug: true,
    src: 'src/',
    dest: 'public/apidoc',
  }, done);
});

gulp.task('sass', function() {
  return gulp.src('./assets/sass/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('sass:watch', function() {
  gulp.watch('./assets/sass/**/*.scss', ['sass']);
});

gulp.task('watch', ['scripts', 'apidoc', 'sass', 'sass:watch'], function() {
  gulp.watch('src/**/*.*', ['scripts', 'apidoc', 'sass', 'sass:watch']);
});

gulp.task('build', ['scripts', 'apidoc', 'sass']);

gulp.task('default', ['watch']);
