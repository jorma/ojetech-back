import * as mocha from 'mocha';
import * as chai from 'chai';

import chaiHttp = require('chai-http');

import app from '../src/App';

chai.use(chaiHttp);
const expect = chai.expect;

describe ('GET /api/loveHoroscope/v001/daily/sagitario/200317', () => {
  it('Should be json', () => {
    chai.request(app).get('/api/loveHoroscope/v001/daily/sagitario/200317')
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res).to.be.json;
        expect(res.type).to.eql('application/json');
        expect(res.body).be.an('object');
      })
  });

  it('Should have a description prop', () => {
    chai.request(app).get('/api/loveHoroscope/v001/daily/sagitario/200317')
      .then(res => {
        expect(res.body.description).to.be.an('string');
      });
  });
});
