#!/usr/bin/env bash
ansible-playbook --ask-pass -i hosts.ini -l production -e "mode=slave" ./provisioning/provision-monitor.yml -vvv
