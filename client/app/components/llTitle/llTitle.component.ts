import {
  Component,
  OnInit,
  Input,
} from '@angular/core';

@Component({
  moduleId: module.id.toString(),
  selector: 'll-title',
  template: `
    <div class="title">
      <h1>{{ this.name }}</h1>
    </div>
  `,
  styleUrls: ['./llTitle.component.scss']
})
export class LlTitle implements OnInit {
  @Input()
  name: string;

  constructor() {}

  ngOnInit() {

  }
}
