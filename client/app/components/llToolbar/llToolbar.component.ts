import {
  Component,
  OnInit,
  Input,
} from '@angular/core';

@Component({
  moduleId: module.id.toString(),
  selector: 'll-toolbar',
  template: `
    <md-toolbar [color]="'primary'">
      {{ this.name }}
    </md-toolbar>
  `,
  styleUrls: ['./llToolbar.component.scss']
})
export class LlToolbarComponent implements OnInit {
  @Input()
  color: string = 'primary';
  @Input()
  name: string = '';

  ngOnInit() {

  }
}
