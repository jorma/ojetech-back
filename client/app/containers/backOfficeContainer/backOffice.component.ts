import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { HoroscopeService } from '../../services/apiHoroscope/apiHoroscope.service';
import { Horoscope } from '../../models/horoscope.model';

@Component({
  moduleId: module.id.toString(),
  selector: 'backoffice-route',
  template: `
    <div class="backOffice">
      <ll-toolbar 
        [name]="name">
      </ll-toolbar
      >
      <ll-title
        [name]="title">
      </ll-title>
      <div class="centered">
        <div class="button">
          Añadir
        </div>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12">
            <table class="table table-hover">
              <thead>
                <td>date</td>
                <td>zodiac</td>
                <td>description</td>
              </thead>
              <tbody>
                <tr *ngFor="let horoscope of horoscopes">
                  <td class="date-cell">{{ horoscope.date | date: 'dd-MM-y' }}</td>
                  <td>{{ horoscope.zodiac }}</td>
                  <td>{{ horoscope.description }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['backOffice.component.scss']
})
export class BackOfficeComponent implements OnInit {
  private horoscopes: Array<Horoscope> = [];
  private name: String = process.env.NODE_ENV === 'production' ? 'Llumyapps Backoffice' : 'Llumyapps Backoffice (development)';
  private title: String = 'Test';
  constructor(private Route: ActivatedRoute, private ApiHoroscope: HoroscopeService) {
  }

  ngOnInit() {
    this.ApiHoroscope.getData().subscribe(
      request => {
        this.horoscopes = request.horoscopes;
      },
      error => {
        this.horoscopes = [];
        console.error('ERROR GETTING HOROSCOPES');
      },() => { }
    );
  }
}
