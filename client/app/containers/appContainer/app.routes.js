"use strict";
var router_1 = require("@angular/router");
var landing_component_1 = require("../landing/landing.component");
var landing2_component_1 = require("../landing2/landing2.component");
var appRoutes = [
    { path: '', redirectTo: 'landing2', pathMatch: 'full' },
    { path: 'landing', component: landing_component_1.LandingComponent },
    { path: 'landing2', component: landing2_component_1.Landing2Component }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
exports.routerComponents = [landing_component_1.LandingComponent];
//# sourceMappingURL=app.routes.js.map