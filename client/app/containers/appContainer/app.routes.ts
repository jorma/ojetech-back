import { Routes, RouterModule } from '@angular/router';
import { BackOfficeComponent } from '../backOfficeContainer/backOffice.component';
import { CounterComponent } from '../../components/counter/counter.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'back-office', pathMatch: 'full' },
  { path: 'back-office', component: BackOfficeComponent },
  { path: 'counter', component: CounterComponent }
];

export const routing = RouterModule.forRoot(appRoutes);

export const routerComponents = [ BackOfficeComponent ];
