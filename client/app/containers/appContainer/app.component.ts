import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app',
  template: `
    <router-outlet></router-outlet>
  `,
  styleUrls: ['styles/root.scss', 'app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
}
