import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';

import { AppComponent } from './containers/appContainer/app.component';
import { BackOfficeComponent } from './containers/backOfficeContainer/backOffice.component';
import { CounterComponent } from './components/counter/counter.component';

import { LlToolbarComponent } from './components/llToolbar/llToolbar.component';
import { LlTitle } from './components/llTitle/llTitle.component';

import { routing } from './containers/appContainer/app.routes'
import { APP_BASE_HREF } from '@angular/common';

import { FormsModule } from '@angular/forms'

import { HoroscopeService } from './services/apiHoroscope/apiHoroscope.service';

import {
  createStore,
  Store,
  StoreEnhancer
} from 'redux';
import { counterReducer } from './reducers/counter.reducer';
import { AppState } from './app.state';
import { AppStore } from './app.store';

let devtools: StoreEnhancer<AppState> = window['devToolsExtension'] ? window['devToolsExtension']() : f => f;

let store: Store<AppState> = createStore<AppState>(
  counterReducer,
  devtools
);

@NgModule({
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot()
  ],
  declarations: [
    AppComponent,
    BackOfficeComponent,
    LlToolbarComponent,
    LlTitle,
    CounterComponent
  ],
  bootstrap: [ AppComponent ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    HoroscopeService,
    { provide: AppStore, useValue: store }
  ]
})
export class AppModule {

}
