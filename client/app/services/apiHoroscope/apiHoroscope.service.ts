import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';

import { Horoscope } from '../../models/horoscope.model';

@Injectable()
export class HoroscopeService {
  private headers = new Headers({ 'Content-Type': 'application/json' });
  private endpoint = process.env.NODE_ENV !== 'production' ? 'www.ojetech.dev' : '';
  private horoscopeUrl = `http://${this.endpoint}/api/dailyHoroscope/v001/all`;

  constructor(private http: Http) { }

  getData() {
    return this.http.get(this.horoscopeUrl).map(this.extractData);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }

  private handleError(error: any) {
    // In a real world appContainer, we might use a remote logging infrastructure
     // We'd also dig deeper into the error to get a better message
     let errMsg = (error.message) ? error.message :
                  error.status ? `${error.status} - ${error.statusText}` : 'Server error';
     console.error(errMsg); // log to console instead
     return Observable.throw(errMsg);
  }
}
