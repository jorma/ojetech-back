"use strict";
if (process.env.NODE_ENV === undefined) {
    process.env.NODE_ENV = 'development';
    console.log(`Environment not defined, using [${process.env.NODE_ENV}] as default`);
}
if (process.env.NODE_ENV !== 'test') {
    require('newrelic');
    console.log(`New Relic [ENABLED]`);
}
else {
    console.log(`New Relic [DISABLED]`);
}
console.log(`Server is running in [${process.env.NODE_ENV}] mode`);
const index_1 = require("./index");
index_1.start();
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = index_1.start;
