"use strict";
const cron_1 = require("cron");
const moment = require("moment");
const request = require("request");
const cheerio = require("cheerio");
const _ = require("lodash");
class LlumCronJobs {
    constructor(storageManager) {
        this.storageManager = storageManager;
        try {
            this.dailyPush = new cron_1.CronJob({
                cronTime: '0 0 0 * * *',
                onTick: () => {
                    const signs = [
                        'aries',
                        'tauro',
                        'capricornio',
                        'sagitarion',
                        'piscis',
                        'escorpio',
                        'libra',
                        'virgo',
                        'cancer',
                        'acuario',
                        'geminis',
                        'leo'
                    ];
                    _(signs).forEach((sign) => {
                        this.sendPush(`${sign}, lee tu horoscopo de hoy`, `Tu horoscopo diario`, sign, '', '#000000', `Tu horoscopo de hoy`, `Tu horoscopo de hoy`);
                    });
                },
                onComplete: () => {
                    return console.log('Complete');
                },
                start: false
            });
            this.dailyScrap = new cron_1.CronJob({
                cronTime: '0 0 4 * * *',
                onTick: () => {
                    const signs = [
                        'Piscis',
                        'Aries',
                        'Capricornio',
                        'Sagitario',
                        'Virgo',
                        'Acuario',
                        'Escorpio',
                        'Leo',
                        'Cancer',
                        'Libra',
                        'Tauro',
                        'Geminis'
                    ];
                    signs.forEach((value) => {
                        let url = `http://www.elhoroscopodehoy.es/horoscopo-manana/${value.toLowerCase()}.php`;
                        request(url, (error, response, html) => {
                            if (error) {
                                console.log('ERROR SCRAP');
                            }
                            else {
                                const $ = cheerio.load(html);
                                let tomorrowScrapped = $('.font12.gray>span').text().replace('Horóscopo del día', '').trim();
                                let tomorrow = moment().add(1, 'days').format('DD/MM/YYYY');
                                if (tomorrowScrapped === tomorrow) {
                                    let description = $('span.prediccion').text();
                                    tomorrow = tomorrow.split('/')[1] + '/' + tomorrow.split('/')[0] + '/' + tomorrow.split('/')[2];
                                    let dateDb = moment(tomorrow).format('YYYY-MM-DD');
                                    this.storageManager.Horoscope.getHoroscope(value, dateDb).then((result) => {
                                        if (result) {
                                            console.log('ALREADY IN DB');
                                        }
                                        else {
                                            this.storageManager.Horoscope.addHoroscope(value, dateDb, description).then((result) => {
                                                console.log('OK');
                                            });
                                        }
                                    });
                                }
                                else {
                                    console.log('YOUR TOMORROW IS NOT MINE');
                                }
                            }
                        });
                    });
                },
                onComplete: () => {
                    return console.log('Complete');
                },
                start: false
            });
        }
        catch (ex) {
            console.log(ex);
        }
    }
    startCrons() {
        this.dailyPush.start();
        this.dailyScrap.start();
        console.log(`Crons started`);
    }
    sendPush(content, heading, tag, largeIcon, accentColor, group, groupMessage) {
        const options = {
            method: 'POST',
            url: 'https://onesignal.com/api/v1/notifications',
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': 'Basic MjM3MjcwNWYtNmNjNi00ZWNlLTg1OTQtMWYzODJlMTgxMzlm'
            },
            body: {
                app_id: 'ba0ef573-abc9-4962-8ba8-62d35b023075',
                contents: {
                    en: content
                },
                headings: {
                    en: heading
                },
                tags: [
                    {
                        key: 'sign',
                        relation: '=',
                        value: tag
                    }
                ],
                delayed_option: 'timezone',
                delivery_time_of_day: '10:00AM',
                large_icon: largeIcon,
                android_accent_color: accentColor,
                android_group: group,
                android_group_message: {
                    en: groupMessage
                },
                data: {
                    description: content
                }
            },
            json: true
        };
        return request(options, (error, response, body) => {
            if (error) {
                return error;
            }
            else {
                return body;
            }
        });
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LlumCronJobs;
