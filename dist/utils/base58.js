"use strict";
class Base58 {
    constructor() {
        this.alphabet = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
        this.base = this.alphabet.length;
    }
    encode(num) {
        let encoded = '';
        while (num) {
            let remainder = num % this.base;
            num = Math.floor(num / this.base);
            encoded = this.alphabet[remainder].toString() + encoded;
        }
        return encoded;
    }
    decode(str) {
        var decoded = 0;
        while (str) {
            var index = this.alphabet.indexOf(str[0]);
            var power = str.length - 1;
            decoded += index * (Math.pow(this.base, power));
            str = str.substring(1);
        }
        return decoded;
    }
}
exports.Base58 = Base58;
