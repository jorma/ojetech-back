"use strict";
class Flash {
    constructor(req) {
        this.req = req;
    }
    addFlash(error, message) {
        this.req.session.flash = {
            error: error,
            message: message
        };
        return this.req;
    }
}
exports.Flash = Flash;
