"use strict";
class HttpRequestError extends Error {
    constructor(status, message) {
        super(message);
        this.status = status;
        this.message = message;
        Object.setPrototypeOf(this, HttpRequestError.prototype);
    }
    getStatus() {
        return this.status;
    }
    getMessage() {
        return this.message;
    }
    beautify(type) {
        switch (type) {
            case 'string':
                return JSON.stringify({ type: this.constructor.name, message: this.getMessage(), status: this.getStatus() });
            default:
                return { type: this.constructor.name, message: this.getMessage(), status: this.getStatus() };
        }
    }
}
exports.HttpRequestError = HttpRequestError;
