"use strict";
const request = require("request");
class LlumAnalytics {
    constructor(trackingCode) {
        this.trackingCode = trackingCode;
    }
    trackEvent(category, action, label, value, cb) {
        const data = {
            v: '1',
            tid: this.trackingCode,
            cid: '000',
            t: 'event',
            ec: category,
            ea: action,
            el: label,
            ev: value
        };
        request.post('http://www.google-analytics.com/collect', {
            form: data,
        }, (err, response) => {
            if (err) {
                cb(err);
                return;
            }
            if (response.statusCode !== 200) {
                cb(new Error('Tracking failed'));
                return;
            }
            cb();
        });
    }
}
exports.LlumAnalytics = LlumAnalytics;
