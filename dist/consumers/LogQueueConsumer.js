"use strict";
const amqp = require("amqplib");
amqp.connect('amqp://ratt:11umyapp5@localhost').then(conn => {
    process.once('SIGINT', () => {
        conn.close();
    });
    return conn.createChannel().then(ch => {
        let ok = ch.assertQueue('log', { durable: true });
        ok = ok.then(() => { ch.prefetch(1); });
        ok = ok.then(() => {
            ch.consume('log', doWork, { noAck: false });
            console.log('LogQueueConsumer [UP]');
        });
        return ok;
        function doWork(msg) {
            let body = msg.content.toString();
            body = JSON.parse(body);
            console.log(body);
            ch.ack(msg);
        }
    });
});
