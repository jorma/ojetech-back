"use strict";
const Sequelize = require("sequelize");
const uuid = require("node-uuid");
class LogManager {
    constructor(sequelize) {
        this.sequelize = sequelize;
        this.Log = this.sequelize.define('Log', {
            id: {
                type: Sequelize.STRING(128),
                allowNull: false,
                primaryKey: true
            },
            hostname: {
                type: Sequelize.STRING(128),
                allowNull: false
            },
            method: {
                type: Sequelize.STRING(16),
                allowNull: false
            },
            url: {
                type: Sequelize.STRING(128),
                allowNull: false
            },
            protocol: {
                type: Sequelize.STRING(32),
                allowNull: false
            },
            ip: {
                type: Sequelize.STRING(64),
                allowNull: false
            }
        }, {
            tableName: 'log',
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        });
    }
    addLog(hostname, method, url, protocol, ip) {
        return this.sequelize.transaction((transaction) => {
            let insertId = uuid.v4();
            return this.Log.create({
                id: insertId,
                hostname: hostname,
                method: method,
                url: url,
                protocol: protocol,
                ip: ip
            }, { transaction: transaction });
        });
    }
}
exports.LogManager = LogManager;
