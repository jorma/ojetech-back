"use strict";
const Sequelize = require("sequelize");
const horoscopeManager_1 = require("./horoscope/horoscopeManager");
const loveHoroscopeManager_1 = require("./loveHoroscope/loveHoroscopeManager");
const logManager_1 = require("./log/logManager");
const urlManager_1 = require("./url/urlManager");
const postManager_1 = require("./post/postManager");
class SequelizeStorageManager {
    constructor(config, cache) {
        this.config = config;
        this.cache = cache;
        this.sequelize = new Sequelize(this.config.database, this.config.username, this.config.password, { dialect: 'mysql', port: this.config.port, host: 'localhost' });
        this.Horoscope = new horoscopeManager_1.HoroscopeManager(this.sequelize, this.cache);
        this.LoveHoroscope = new loveHoroscopeManager_1.LoveHoroscopeManager(this.sequelize);
        this.Log = new logManager_1.LogManager(this.sequelize);
        this.Url = new urlManager_1.UrlManager(this.sequelize);
        this.Post = new postManager_1.PostManager(this.sequelize);
    }
    init(force) {
        force = force || false;
        if (process.env.NODE_ENV === 'production') {
            return this.sequelize.sync({
                force: force,
                logging: () => false,
            });
        }
        else {
            return this.sequelize.sync({
                force: force,
            });
        }
    }
}
exports.SequelizeStorageManager = SequelizeStorageManager;
