"use strict";
const Sequelize = require("sequelize");
class UrlManager {
    constructor(sequelize) {
        this.sequelize = sequelize;
        this.Url = this.sequelize.define('Url', {
            id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                primaryKey: true,
                autoIncrement: true
            },
            longUrl: {
                type: Sequelize.TEXT,
                allowNull: false
            },
            clicks: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 0
            }
        }, {
            tableName: 'url',
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        });
    }
    findByLongUrl(longUrl) {
        return this.Url.find({ where: { longUrl: longUrl } });
    }
    insertUrl(longUrl) {
        return this.sequelize.transaction((transaction) => {
            return this.Url.create({ longUrl: longUrl }, { transaction: transaction });
        });
    }
    findById(id) {
        return this.Url.find({ where: { id: id } });
    }
}
exports.UrlManager = UrlManager;
