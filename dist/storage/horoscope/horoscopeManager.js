"use strict";
const Sequelize = require("sequelize");
const uuid = require("node-uuid");
const moment = require("moment");
class HoroscopeManager {
    constructor(sequelize, cache) {
        this.sequelize = sequelize;
        this.cache = cache;
        this.Horoscope = this.sequelize.define('Horoscope', {
            id: {
                type: Sequelize.STRING(128),
                allowNull: false,
                primaryKey: true
            },
            zodiac: {
                type: Sequelize.STRING(128),
                allowNull: false,
            },
            date: {
                type: Sequelize.DATEONLY,
                allowNull: false
            },
            description: {
                type: Sequelize.TEXT(),
                allowNull: false
            }
        }, {
            tableName: 'horoscope',
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        });
    }
    destroyById(id) {
        return this.Horoscope.find({ where: { id: id } }).then((horoscope) => {
            let dateDb = moment(horoscope.date).format('YYYY-MM-DD');
            this.cache.del(`daily-${horoscope.zodiac.toLowerCase()}-${dateDb}`, (err, num) => {
            });
            horoscope.destroy().thenReturn();
        }).catch((error) => {
        });
    }
    getHoroscope(zodiac, date) {
        return this.Horoscope.find({ where: { zodiac: zodiac, date: date } });
    }
    getById(id) {
        return this.Horoscope.findOne({
            where: {
                id: id
            }
        });
    }
    addHoroscope(zodiac, date, description) {
        return this.sequelize.transaction((transaction) => {
            let insertId = uuid.v4();
            return this.Horoscope.create({
                id: insertId,
                zodiac: zodiac,
                date: date,
                description: description
            }, { transaction: transaction });
        });
    }
    getAllHoroscopes() {
        return this.Horoscope.findAll();
    }
    getPagedHoroscopes(offset) {
        const limit = 24;
        return this.Horoscope.findAndCountAll({
            limit: limit,
            offset: (offset - 1) * limit,
            order: [
                ['date', 'DESC']
            ]
        });
    }
}
exports.HoroscopeManager = HoroscopeManager;
