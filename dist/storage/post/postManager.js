"use strict";
const Sequelize = require("sequelize");
const uuid = require("node-uuid");
class PostManager {
    constructor(sequelize) {
        this.sequelize = sequelize;
        this.Post = this.sequelize.define('Post', {
            id: {
                type: Sequelize.STRING(128),
                allowNull: false,
                primaryKey: true
            },
            title: {
                type: Sequelize.STRING(256),
                allowNull: false,
            },
            mainPicture: {
                type: Sequelize.STRING(256),
                allowNull: false,
                defaultValue: ''
            },
            content: {
                type: Sequelize.TEXT,
                allowNull: true
            },
            slug: {
                type: Sequelize.STRING(128),
                allowNull: false
            }
        }, {
            tableName: 'post',
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        });
    }
    getPagedPosts(offset, limit) {
        return this.Post.findAndCountAll({
            limit: limit,
            offset: (offset - 1) * limit,
            order: [
                ['date', 'DESC']
            ]
        });
    }
    addPost(title, mainPicture, content, slug) {
        return this.sequelize.transaction((transaction) => {
            let insertId = uuid.v4();
            return this.Post.create({
                id: insertId,
                title: title,
                mainPicture: mainPicture,
                content: content,
                slug: slug
            }, { transaction: transaction });
        });
    }
    getSinglePost(slug) {
        return this.Post.findOne({
            where: {
                slug: slug
            }
        });
    }
}
exports.PostManager = PostManager;
