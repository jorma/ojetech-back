"use strict";
const Sequelize = require("sequelize");
const uuid = require("node-uuid");
class LoveHoroscopeManager {
    constructor(sequelize) {
        this.sequelize = sequelize;
        this.LoveHoroscope = this.sequelize.define('LoveHoroscope', {
            id: {
                type: Sequelize.STRING(128),
                allowNull: false,
                primaryKey: true
            },
            zodiac: {
                type: Sequelize.STRING(128),
                allowNull: false
            },
            date: {
                type: Sequelize.DATEONLY,
                allowNull: false
            },
            description: {
                type: Sequelize.TEXT(),
                allowNull: false
            }
        }, {
            tableName: 'lovehoroscope',
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        });
    }
    getLoveHoroscope(zodiac, date) {
        return this.LoveHoroscope.find({ where: { zodiac: zodiac, date: date } });
    }
    addLoveHoroscope(zodiac, date, description) {
        return this.sequelize.transaction((transaction) => {
            let insertId = uuid.v4();
            return this.LoveHoroscope.create({
                id: insertId,
                zodiac: zodiac,
                date: date,
                description: description
            }, { transaction: transaction });
        });
    }
}
exports.LoveHoroscopeManager = LoveHoroscopeManager;
