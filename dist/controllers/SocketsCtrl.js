"use strict";
const BaseCtrl_1 = require("./BaseCtrl");
class SocketsCtrl extends BaseCtrl_1.BaseController {
    constructor(storageManager, app) {
        super(app);
        this.storageManager = storageManager;
        let io = app.get('sockets');
        io.on('connection', (socket) => {
            socket.on('disconnect', () => {
                console.log('User disconnected');
            });
            socket.on('add_message', (message) => {
                io.emit('message', { type: 'new-message', text: message });
            });
            socket.on('get_requests', () => {
                io.emit('requests', { type: 'number_request', number: 2 });
            });
        });
    }
}
exports.SocketsCtrl = SocketsCtrl;
