"use strict";
const BaseCtrl_1 = require("./BaseCtrl");
const base58_1 = require("../utils/base58");
const config_1 = require("../config/config");
class UrlCtrl extends BaseCtrl_1.BaseController {
    constructor(app) {
        super(app);
        this.insertLog = (req, res, next) => {
            this.storageManager.Log.addLog(req.hostname, req.method, req.url, req.protocol, req.headers['x-forwarded-for'] || req.ip);
            return next();
        };
        this.shortUrl = (req, res) => {
            let longUrl = req.body.url;
            let shortUrl = '';
            if (!longUrl)
                return res.status(404).json({ description: 'url cannot be null' });
            if (longUrl.indexOf('http') === -1)
                longUrl = `http://${longUrl}`;
            return this.storageManager.Url.findByLongUrl(longUrl).then((row) => {
                if (!row) {
                    return this.storageManager.Url.insertUrl(longUrl).then((row) => {
                        shortUrl = this.base58.encode(row.id);
                        return res.status(200).json({ shortUrl: config_1.default[process.env.NODE_ENV || 'development'].domain + '/l/' + shortUrl });
                    });
                }
                return res.status(200).json({ shortUrl: config_1.default[process.env.NODE_ENV || 'development'].domain + '/l/' + this.base58.encode(row.id) });
            });
        };
        this.decodeUrl = (req, res) => {
            if (!req.params.encoded)
                return res.redirect(config_1.default[process.env.NODE_ENV || 'development'].domain);
            let base58Id = req.params.encoded;
            let id = this.base58.decode(base58Id);
            this.storageManager.Url.findById(id).then((row) => {
                if (!row) {
                    return res.redirect(config_1.default[process.env.NODE_ENV || 'development'].domain);
                }
                // Añadir click a la URL
                return res.redirect(row.longUrl);
            });
        };
        this.storageManager = app.get('storageManager');
        this.base58 = new base58_1.Base58();
        app.post('/api/shorten/v001/short', this.insertLog, this.shortUrl);
        app.get('/l/:encoded', this.insertLog, this.decodeUrl);
    }
}
exports.UrlCtrl = UrlCtrl;
