"use strict";
const BaseCtrl_1 = require("./BaseCtrl");
class PostCtrl extends BaseCtrl_1.BaseController {
    constructor(app) {
        super(app);
        this.insertLog = (req, res, next) => {
            this.storageManager.Log.addLog(req.hostname, req.method, req.url, req.protocol, req.headers['x-forwarded-for'] || req.ip);
            next();
        };
        this.getArticles = (req, res, next) => {
            let page = req.params.page ? req.params.page : 1;
            const limit = 10;
            return this.storageManager.Post.getPagedPosts(page, limit).then((results) => {
                const posts = results.rows;
                const pages = Math.ceil(results.count / limit);
                return res.render('blog/wall', {
                    title: 'Ojetech - Blog',
                    posts,
                    pages,
                    page: parseInt(page)
                });
            });
        };
        this.getArticle = (req, res, next) => {
            const slug = req.params.slug;
            return this.storageManager.Post.getSinglePost(slug).then((result) => {
                return res.render('blog/singlePost', {
                    title: `Ojetech - Blog - ${result.title}`,
                    articleTitle: result.title,
                    slug: result.slug,
                    content: result.content
                });
            });
        };
        this.storageManager = app.get('storageManager');
        app.get('/articles/:page?', this.insertLog, this.getArticles);
        app.get('/article/:slug', this.insertLog, this.getArticle);
    }
}
exports.PostCtrl = PostCtrl;
