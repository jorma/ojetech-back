"use strict";
const moment = require("moment");
const BaseCtrl_1 = require("./BaseCtrl");
const llumyErrors_1 = require("../utils/llumyErrors");
const os = require("os");
const cheerio = require("cheerio");
const request = require("request");
const Promise = require("bluebird");
class GeneralCtrl extends BaseCtrl_1.BaseController {
    constructor(app, cache) {
        super(app);
        this.newPostView = (req, res, next) => {
            return res.render('admin/articles/new', {
                title: 'Llumyapps - Admin (Posts) - New',
                user: req.session,
            });
        };
        this.auth = (req, res, next) => {
            if (req.session && req.session.username === 'root' && req.session.admin) {
                res.locals.path = req.path;
                return next();
            }
            else {
                res.redirect('/admin');
                return this.influx.writePoints([
                    {
                        measurement: 'unauthorized_admin',
                        tags: { host: os.hostname() },
                        fields: { path: req.path, count: 1 },
                    }
                ]).catch((error) => {
                    console.log('ERROR ' + error.stack);
                });
            }
        };
        this.getContact = (req, res, next) => {
            return res.render('contact', { title: 'Ojetech - contact', message: 'Hello there!' });
        };
        this.getServices = (req, res, next) => {
            return res.render('services', { title: 'Ojetech - services', message: 'Hello there!' });
        };
        this.getHoroscopeTomorrow = (req, res, next) => {
            let promises = [];
            const signs = [
                'Piscis',
                'Aries',
                'Capricornio',
                'Sagitario',
                'Virgo',
                'Acuario',
                'Escorpio',
                'Leo',
                'Cancer',
                'Libra',
                'Tauro',
                'Geminis'
            ];
            signs.forEach((value) => {
                let url = `http://www.elhoroscopodehoy.es/horoscopo-manana/${value.toLowerCase()}.php`;
                promises.push(this.getHoroscopeFromUrl(url, value));
            });
            Promise.all(promises).then(() => res.redirect('/admin/dashboard/horoscope'));
        };
        this.deleteHoroscope = (req, res, next) => {
            let id = req.params.id;
            this.storageManager.Horoscope.destroyById(id).then((result) => {
                req.flash('info', 'Borrado con éxito');
                return res.redirect('/admin/dashboard/horoscope/1');
            }).catch((error) => {
                return res.json(error);
            });
        };
        this.newHoroscopeView = (req, res, next) => {
            return res.render('admin/horoscopes/new', {
                title: 'Llumyapps - Admin (Horoscopes) - New',
                user: req.session,
            });
        };
        this.newHoroscope = (req, res, next) => {
            let { inputZodiac, inputDate, inputDescription } = req.body;
            let dateDb = moment(`20${inputDate.slice(-2)}-${inputDate.slice(2, 4)}-${inputDate.slice(0, 2)}`).format('YYYY-MM-DD');
            return this.storageManager.Horoscope.addHoroscope(inputZodiac, dateDb, inputDescription).then((result) => {
                if (result) {
                    return res.redirect('/admin/dashboard/horoscope/view/' + result.id);
                }
            }).catch((error) => {
                return res.json(error);
            });
        };
        this.updateHoroscope = (req, res, next) => {
            let id = req.params.id;
            let { inputZodiac, inputDate, inputDescription } = req.body;
            let dateDb = moment(`20${inputDate.slice(-2)}-${inputDate.slice(2, 4)}-${inputDate.slice(0, 2)}`).format('YYYY-MM-DD');
            return this.storageManager.Horoscope.getById(id).then((result) => {
                this.cache.del(`daily-${result.zodiac.toLowerCase()}-${moment(result.date).format('YYYY-MM-DD')}`, (error, num) => {
                    if (error) {
                        console.log(error);
                    }
                    else {
                        console.log(num);
                    }
                });
                result.update({
                    zodiac: inputZodiac,
                    date: dateDb,
                    description: inputDescription,
                }).then((result) => {
                    res.redirect('/admin/dashboard/horoscope/view/' + result.id);
                }).catch((error) => {
                    res.json(error);
                });
            }).catch((error) => {
                res.json(error);
            });
        };
        this.getHoroscope = (req, res, next) => {
            let id = req.params.id;
            return this.storageManager.Horoscope.getById(id).then((result) => {
                return res.render('admin/horoscopes/view', {
                    title: 'Llumyapps - Admin (Horoscopes) - Detail',
                    user: req.session,
                    horoscope: result
                });
            }).catch((error) => {
                console.log(error);
            });
        };
        this.getDashboardHoroscope = (req, res, next) => {
            let page = req.params.page ? req.params.page : 1;
            return this.storageManager.Horoscope.getPagedHoroscopes(page).then((results) => {
                const horoscopes = results.rows;
                const pages = Math.ceil(results.count / 24.0);
                return res.render('admin/horoscopes', {
                    title: 'Llumyapps - Admin (Horoscopes)',
                    flash: {
                        info: req.flash('info'),
                        error: req.flash('error'),
                    },
                    horoscopes,
                    pages,
                    page: parseInt(page),
                });
            });
        };
        this.getDashboardBlog = (req, res, next) => {
            let page = req.params.page ? req.params.page : 1;
            const limit = 24;
            return this.storageManager.Post.getPagedPosts(page, limit).then((results) => {
                const posts = results.rows;
                const pages = Math.ceil(results.count / 24.0);
                return res.render('admin/articles', {
                    title: 'Llumyapps - Admin (Posts)',
                    flash: {
                        info: req.flash('info'),
                        error: req.flash('error')
                    },
                    posts,
                    pages,
                    page: parseInt(page)
                });
            });
        };
        this.getDashboard = (req, res, next) => {
            return res.redirect('/admin/dashboard/horoscope');
        };
        this.login = (req, res, next) => {
            if (req.body.username &&
                req.body.username === 'root' &&
                req.body.password &&
                req.body.password === 's3dT35t1605') {
                req.session.username = 'root';
                req.session.admin = true;
                return res.redirect('/admin/dashboard');
            }
            else {
                req.session.username = undefined;
                req.session.admin = false;
                return res.redirect('/admin');
            }
        };
        this.insertLog = (req, res, next) => {
            if (process.env.NODE_ENV !== 'all') {
                req.producerService.publish('log', {
                    hostname: req.hostname,
                    method: req.method,
                    url: req.url,
                    protocol: req.protocol
                });
            }
            this.storageManager.Log.addLog(req.hostname, req.method, req.url, req.protocol, req.headers['x-forwarded-for'] || req.ip);
            next();
        };
        this.getHeartbeat = (req, res) => {
            return res.status(200).json({ description: 'Heartbeat ok' });
        };
        this.getWall = (req, res) => {
            return res.render('wall', { title: 'Llumyapps', message: 'Hello there!' });
        };
        this.getRoot = (req, res) => {
            return res.render('index', { title: 'Llumyapps', message: 'Hello there!' });
        };
        this.getCabra = (req, res) => {
            return res.render('lacabra', { title: 'Beroomers' });
        };
        this.getRobots = (req, res) => {
            res.type('text/plain');
            return res.send('User-agent: *\nDisallow: /');
        };
        this.getAdmin = (req, res) => {
            return res.render('admin/login', { title: 'Llumyapps - Admin' });
        };
        this.fallback = (req, res) => {
            throw new llumyErrors_1.HttpRequestError(404, 'Not found');
        };
        this.storageManager = app.get('storageManager');
        this.influx = app.get('influx');
        this.cache = cache;
        app.get('heartbeat', '/heartbeat', this.insertLog, this.getHeartbeat);
        app.get('wall', '/', this.getWall);
        app.get('contact', '/contact', this.insertLog, this.getContact);
        app.get('services', '/services', this.insertLog, this.getServices);
        app.get('admin', '/admin', this.insertLog, this.getAdmin);
        app.get('cabra', '/lacabra', this.insertLog, this.getCabra);
        app.get('robots', '/robots.txt', this.insertLog, this.getRobots);
        app.post('admin.login', '/admin/login', this.insertLog, this.login);
        app.get('admin.dashboard', '/admin/dashboard', this.auth, this.getDashboard);
        app.get('admin.dashboard.horoscope.new', '/admin/dashboard/horoscope/new', this.auth, this.newHoroscopeView);
        app.post('post_admin.dashboard.horoscope.new', '/admin/dashboard/horoscope/new', this.auth, this.newHoroscope);
        app.get('admin.dashboard.horoscope.view', '/admin/dashboard/horoscope/view/:id', this.auth, this.getHoroscope);
        app.post('post_admin.dashboard.horoscope.view', '/admin/dashboard/horoscope/view/:id', this.auth, this.updateHoroscope);
        app.get('admin.dashboard.horoscope.delete', '/admin/dashboard/horoscope/delete/:id', this.auth, this.deleteHoroscope);
        app.get('admin.dashboard.horoscope', '/admin/dashboard/horoscope/:page?', this.auth, this.getDashboardHoroscope);
        app.get('admin.dashboard.horoscope.tomorrow.scrap', '/admin/dashboard/horoscope/tomorrow/scrap', this.auth, this.getHoroscopeTomorrow);
        app.get('admin.dashboard.post', '/admin/dashboard/blog', this.auth, this.getDashboardBlog);
        app.get('admin.dashboard.post.new', '/admin/dashboard/blog/new', this.auth, this.newPostView);
        app.get('*', this.fallback);
    }
    getHoroscopeFromUrl(url, value) {
        return new Promise((resolve) => {
            request(url, (error, response, html) => {
                if (!error) {
                    const $ = cheerio.load(html);
                    let tomorrowScrapped = $('.font12.gray>span').text().replace('Horóscopo del día', '').trim();
                    let tomorrow = moment().add(1, 'days').format('DD/MM/YYYY');
                    if (tomorrowScrapped === tomorrow) {
                        let description = $('span.prediccion').text();
                        tomorrow = tomorrow.split('/')[1] + '/' + tomorrow.split('/')[0] + '/' + tomorrow.split('/')[2];
                        let dateDb = moment(tomorrow).format('YYYY-MM-DD');
                        this.storageManager.Horoscope.getHoroscope(value, dateDb).then((result) => {
                            if (!result) {
                                this.storageManager.Horoscope.addHoroscope(value, dateDb, description).then(resolve);
                            }
                            else {
                                resolve();
                            }
                        });
                    }
                    else {
                        resolve();
                    }
                }
            });
        });
    }
}
exports.GeneralCtrl = GeneralCtrl;
