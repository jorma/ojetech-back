"use strict";
const awilix_express_1 = require("awilix-express");
const dependencies_1 = require("../config/dependencies");
class BaseController {
    constructor(app) {
        dependencies_1.dependencies.forEach(element => {
            app.use(awilix_express_1.inject(element.name));
        });
    }
}
exports.BaseController = BaseController;
