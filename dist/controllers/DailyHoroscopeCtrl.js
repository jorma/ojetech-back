"use strict";
const request = require("request");
const cheerio = require("cheerio");
const moment = require("moment");
const BaseCtrl_1 = require("./BaseCtrl");
class DailyHoroscopeCtrl extends BaseCtrl_1.BaseController {
    constructor(app, cache) {
        super(app);
        this.insertLog = (req, res, next) => {
            this.storageManager.Log.addLog(req.hostname, req.method, req.url, req.protocol, req.headers['x-forwarded-for'] || req.ip);
            next();
        };
        this.getDailyHoroscope = (req, res) => {
            const zodiac = req.params.zodiac, date = req.params.date;
            const url = `http://www.horoscopodiariodehoy.com/horoscopo-de-hoy-${zodiac}-${date}`;
            let dateDb = moment(`20${date.slice(-2)}-${date.slice(2, 4)}-${date.slice(0, 2)}`).format('YYYY-MM-DD');
            this.storageManager.Horoscope.getHoroscope(zodiac, dateDb).then((horoscope) => {
                if (horoscope) {
                    return res.status(200).json({
                        description: horoscope.description,
                        cacheable: true,
                    });
                }
                return request(url, (error, response, html) => {
                    if (error)
                        return res.status(500).json(error);
                    const $ = cheerio.load(html);
                    let text = $('.entry-content>p').text();
                    if (text !== '') {
                        this.storageManager.Horoscope.addHoroscope(zodiac, dateDb, text);
                        return res.status(200).json({
                            description: text,
                            cacheable: true
                        });
                    }
                    dateDb = moment(`20${date.slice(-2)}-${date.slice(2, 4)}-${date.slice(0, 2)}`).subtract('days', 1).format('YYYY-MM-DD');
                    this.storageManager.Horoscope.getHoroscope(zodiac, dateDb).then((horoscope) => {
                        if (horoscope) {
                            return res.status(200).json({
                                description: horoscope.description,
                                cacheable: false
                            });
                        }
                        return res.status(200).json({
                            description: 'Algo no ha ido bien y hoy es imposible predecir tu día, lo sentimos muchísimo, nuestros videntes están trabajando en ello',
                            cacheable: false
                        });
                    });
                });
            });
        };
        this.storageManager = app.get('storageManager');
        this.cache = cache;
        /**
         * @api {get} /dailyHoroscope/v001/daily/:zodiac/:date
         * @apiName GetHoroscope
         * @apiGroup DailyHoroscope
         *
         * @apiParam {String} zodiac Sign of horoscope
         * @apiParam {String} date Date of the request in format (DDMMYY)
         *
         * @apiSuccess {String} description The horoscope for this day
         * @apiSuccess {Boolean} cacheable Is cacheable the result?
         *
         * @apiSuccessExample Success-Response:
         *  HTTP/1.1 200 OK
         *  {
         *    "description": "Today will be a great day",
         *    "cacheable": true
         *  }
         */
        app.get('/api/dailyHoroscope/v001/daily/:zodiac/:date', (req, res, next) => {
            let date = req.params.date;
            date = moment(`20${date.slice(-2)}-${date.slice(2, 4)}-${date.slice(0, 2)}`).format('YYYY-MM-DD');
            res.express_redis_cache_name = `daily-${req.params.zodiac.toLowerCase()}-${date}`;
            next();
        }, this.cache.route(), this.insertLog, this.getDailyHoroscope);
    }
}
exports.DailyHoroscopeCtrl = DailyHoroscopeCtrl;
