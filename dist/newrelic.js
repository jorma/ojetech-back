'use strict';
/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
const appName = process.env.NODE_ENV === 'production' ? 'llumyapps-prod' : 'llumyapps-dev';
console.log(`New Relic connected to [${appName}]`);
exports.config = {
    /**
     * Array of application names.
     */
    app_name: [appName],
    /**
     * Your New Relic license key.
     */
    license_key: '26e297f4b00a59163cdcfd9d343289f8bb8e41eb',
    logging: {
        /**
         * Level at which to log. 'trace' is most useful to New Relic when diagnosing
         * issues with the agent, 'info' and higher will impose the least overhead on
         * production applications.
         */
        level: 'info'
    }
};
