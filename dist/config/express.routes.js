"use strict";
const express = require("express");
const path = require("path");
const Promise = require("bluebird");
const DailyHoroscopeCtrl_1 = require("../controllers/DailyHoroscopeCtrl");
const LoveHoroscopeCtrl_1 = require("../controllers/LoveHoroscopeCtrl");
const UrlCtrl_1 = require("../controllers/UrlCtrl");
const GeneralCtrl_1 = require("../controllers/GeneralCtrl");
const PostCtrl_1 = require("../controllers/PostCtrl");
let cache = require('express-redis-cache')({
    expires: 3600
});
function configureRoutes(app) {
    return new Promise((resolve) => {
        new DailyHoroscopeCtrl_1.DailyHoroscopeCtrl(app, cache);
        new LoveHoroscopeCtrl_1.LoveHoroscopeCtrl(app);
        new UrlCtrl_1.UrlCtrl(app);
        new PostCtrl_1.PostCtrl(app);
        app.use('/assets', express.static(path.join(__dirname, '../../assets')));
        app.use('/resources', express.static(path.join(__dirname, '../../public')));
        app.use('/back-office', express.static(path.join(__dirname, '../../public')));
        app.use('/apidoc', express.static(path.join(__dirname, '../../public/apidoc')));
        new GeneralCtrl_1.GeneralCtrl(app, cache);
        resolve();
    });
}
exports.configureRoutes = configureRoutes;
