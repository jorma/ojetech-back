"use strict";
const Promise = require("bluebird");
const Raven = require("raven");
const llumyErrors_1 = require("../utils/llumyErrors");
const llumAnalytics_1 = require("../utils/llumAnalytics");
let analytics = new llumAnalytics_1.LlumAnalytics('UA-83291516-5');
function configureErrorHandlers(app) {
    return new Promise((resolve) => {
        app.use(function (err, req, res, next) {
            if (err) {
                if (err.name === 'UnauthorizedError') {
                    res.status(401).json({
                        code: 401,
                        description: 'Invalid access token'
                    });
                }
                else if (err instanceof llumyErrors_1.HttpRequestError) {
                    if (process.env.NODE_ENV === 'production') {
                        analytics.trackEvent('Log', 'Error', 'HttpRequestError', 1, (err) => { });
                        Raven.captureException(err.beautify('string'), { req: req });
                    }
                    res.status(err.getStatus()).json(err.beautify());
                }
                else {
                    if (process.env.NODE_ENV !== 'production') {
                        res.status(500).json({ error: err.stack });
                    }
                    else {
                        res.status(500).render('error', { title: 'Llumyapps - Ups!' });
                    }
                }
            }
            next();
        });
        resolve();
    });
}
exports.configureErrorHandlers = configureErrorHandlers;
