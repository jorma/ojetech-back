"use strict";
const LogService_1 = require("../services/LogService");
const awilix_1 = require("awilix");
const ProducerService_1 = require("../services/ProducerService");
let dependencies;
exports.dependencies = dependencies;
if (process.env.NODE_ENV !== 'all') {
    exports.dependencies = dependencies = [
        {
            name: 'logService',
            service: LogService_1.LogService,
            type: 'class',
            lifetime: awilix_1.Lifetime.SCOPED
        },
        {
            name: 'producerService',
            service: ProducerService_1.ProducerService,
            type: 'class',
            lifetime: awilix_1.Lifetime.SCOPED
        }
    ];
}
else {
    exports.dependencies = dependencies = [
        {
            name: 'logService',
            service: LogService_1.LogService,
            type: 'class',
            lifetime: awilix_1.Lifetime.SCOPED
        }
    ];
}
