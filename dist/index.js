"use strict";
const socketio = require("socket.io");
const Promise = require("bluebird");
const config_1 = require("./config/config");
const storage_1 = require("./storage/storage");
const SocketsCtrl_1 = require("./controllers/SocketsCtrl");
const Cron_1 = require("./crons/Cron");
const llumAnalytics_1 = require("./utils/llumAnalytics");
const express_1 = require("./config/express");
const express_routes_1 = require("./config/express.routes");
const express_errorHandlers_1 = require("./config/express.errorHandlers");
const express_influx_1 = require("./config/express.influx");
let analytics = new llumAnalytics_1.LlumAnalytics('UA-83291516-5');
let cache = require('express-redis-cache')({
    expires: 3600
});
function start() {
    analytics.trackEvent('Log', 'Info', 'Start', 1, (err) => { });
    let storageManager = new storage_1.SequelizeStorageManager({
        database: config_1.default[process.env.NODE_ENV || 'development'].database,
        username: config_1.default[process.env.NODE_ENV || 'development'].username,
        password: config_1.default[process.env.NODE_ENV || 'development'].password,
        port: config_1.default[process.env.NODE_ENV || 'development'].port
    }, cache);
    return express_influx_1.influx.getDatabaseNames().then((names) => {
        if (names.indexOf('express_response_db') === -1) {
            return express_influx_1.influx.createDatabase('express_response_db');
        }
    }).then(() => {
        return storageManager.init().then(() => {
            return express_1.configureExpress(storageManager, express_influx_1.influx).then((app) => {
                return express_routes_1.configureRoutes(app).then(() => {
                    return express_errorHandlers_1.configureErrorHandlers(app).then(() => {
                        return app;
                    });
                });
            }).then((app) => {
                return new Promise((resolve) => {
                    var server = app.listen(1196, () => {
                        if (process.env.NODE_ENV === 'production') {
                            let crons = new Cron_1.default(storageManager);
                            crons.startCrons();
                        }
                        let io = socketio(server);
                        app.set('sockets', io);
                        let socketsController = new SocketsCtrl_1.SocketsCtrl(storageManager, app);
                        resolve(server);
                    });
                });
            });
        });
    }).catch((err) => {
        console.error(err);
        console.error(`Error creating Influx database`);
    });
}
exports.start = start;
