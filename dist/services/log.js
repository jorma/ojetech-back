"use strict";
const bunyan_1 = require("bunyan");
class Log {
    constructor() {
        this.log = bunyan_1.createLogger({
            name: 'global'
        });
    }
}
exports.Log = Log;
