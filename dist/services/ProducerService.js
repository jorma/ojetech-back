"use strict";
const amqp = require("amqplib");
class ProducerService {
    constructor() {
    }
    publish(queue, message) {
        const messageToQueue = { payload: message };
        amqp.connect('amqp://ratt:11umyapp5@localhost').then(conn => {
            return conn.createChannel().then(ch => {
                const q = queue;
                let ok = ch.assertQueue(q, { durable: true });
                return ok.then(() => {
                    let msg = process.argv.slice(2).join(' ') || JSON.stringify(messageToQueue);
                    ch.sendToQueue(q, new Buffer(msg), { deliveryMode: true });
                    console.log(`[x] Sent '${JSON.stringify(message)}' to queue '${queue}'`);
                    return ch.close();
                });
            }).finally(() => {
                conn.close();
            });
        }).catch(console.warn);
    }
}
exports.ProducerService = ProducerService;
