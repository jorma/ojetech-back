"use strict";
const bunyan_1 = require("bunyan");
class LogService {
    constructor() {
        this.log = bunyan_1.createLogger({
            name: 'internal'
        });
    }
    getLog() {
        return this.log;
    }
}
exports.LogService = LogService;
