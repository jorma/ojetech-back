"use strict";
const bunyan_1 = require("bunyan");
const log = bunyan_1.createLogger({
    name: 'global',
});
exports.log = log;
