#!/usr/bin/env bash
ansible-playbook --ask-pass -i hosts.ini -l production ./provisioning/provision-redis-playbook.yml -vvv
