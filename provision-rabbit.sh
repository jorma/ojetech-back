#!/usr/bin/env bash
ansible-playbook --ask-pass -i hosts.ini -l production ./provisioning/provision-rabbitmq.yml -vvv
