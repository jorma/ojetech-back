#!/usr/bin/env node
var SecureConf = require('secure-conf');
var sconf = new SecureConf();

sconf.encryptFile(
  "./dist/config/configVars.json",
  "./dist/config/configVars.json.enc",
  function(err, f, ef, ec) {
    if (err) {
      console.log("Failed to encrypt %s, error is %s", f, err);
    } else {
      console.log("Encrypt %s to %s complete.", f, ef);
      console.log("Encrypted contents are %s", ec);
      sconf.decryptFile("./dist/config/configVars.json.enc", function(err, file, content) {
        if (err) {
          console.log("Unable to retrieve the configuration contents.");
        } else {
          var config = JSON.parse(content);
          console.log(config);
        }
      });
    }
  }
);
