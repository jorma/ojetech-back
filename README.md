# Llumyapps backend
### Sumary
This project consists in an API Rest for an Android app that serves your daily horoscope, it is responsible of sending daily push notifications and daily predictions and give persistence to this predictions.

### Technologies
This project is wrote in **NodeJS** below **TypeScript**, specifically with the **ExpressJS** framework guided by the **MVC** philosophy. For persistence I used **MySQL** with the **ORM named Sequelize**, also has a **Rethinkdb** database for **real time connections and publisher-subscriber model**. To allow the system to send **push notifications** I use an external service provided by OneSignal, so the system is fully integrated with their platform.

To **scrap** the different websites for get the daily predictions I used the **Cheerio** library, that allow me to search into the DOM like using jQuery in the frontend

This project is entirely wrote in **TypeScript** and transpiled with tools like **Gulp or Webpack**, also it have support for real time connections, with **Socket.io** and Rethinkdb, it has also a frontend part wrote in **Angular2 (TypeScript)** but only for learning purposes.

### Architecture
This project is hosted in a server in OVH (Frankfurt) with **Ubuntu OS**. The server is wrote in NodeJS and listen in a different port of 80 or 443, a **NginX** server is responsible of the redirection, acting as a **proxy**, the database is hosted in the same machine.

The external security of the server is managed by **Iptables** directly, with a well configured Iptables that only allow connections at the necessary ports and the true system is not in this port, as I mentioned before, the service in public ports acts like a proxy. The uploads of new versions are managed by **Git**, I don't have FTP Server for uploads, I don't trust in this technology for this task.

### Contact
If you are interested in learn more about this system, don't hesitate to contact me at.
Email: yejorge@hotmail.com
