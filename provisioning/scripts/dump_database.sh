#!/bin/bash

filename=`date +%Y%m%d%H%M%S`

mysqldump -u root -pvagrant llumyapps > /var/llumyapps_backups/$filename.sql
tar -cvzf /var/llumyapps_backups/$filename.tgz /var/llumyapps_backups/$filename.sql
rm /var/llumyapps_backups/$filename.sql
