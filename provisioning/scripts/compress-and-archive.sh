#!/bin/bash
#
#Backup Policy
#=============
#
#The backups will follow the next scheme:
#
#1. Current Month: 1 dump each hour
#2. Previous Month: 1 dump each hour
#3. Older Months: 1 dump each day
#
#Get files from two months ago
if [ -z "$1" ]
  then
    tarFilename=`date +%B-%Y -d "-2 month"`;
    sqlFiles=`date +%Y%m -d "-2 month"`;
else
  tarFilename=`date +%B-%Y -d "$1 -2 month"`;
  sqlFiles=`date +%Y%m -d "$1 -2 month"`;
fi

echo "Creating $tarFilename from $sqlFiles files";

#Packing last file of each day of that month
cd /var/llumyapps_backups/mysql/;
tar -cvzf /var/llumyapps_backups/mysql/archived/$tarFilename.tgz `ls $sqlFiles??230001.tgz|xargs`

#Remove all the dumps of that month
rm -rf /var/llumyapps_backups/mysql/$sqlFiles????????.tgz
