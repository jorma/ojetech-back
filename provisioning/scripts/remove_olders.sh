#!/bin/bash

for file in `(ls -t /var/llumyapps_backups|head -n 48;ls)|grep tgz|sort|uniq -u|xargs`
do
  rm /var/llumyapps_backups/$file;
done;
