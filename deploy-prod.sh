#!/usr/bin/env bash
ansible-playbook --ask-pass -i hosts.ini -l production ./deploy/deploy-playbook.yml -vvv
