import * as request from 'request';

export class LlumAnalytics {
  trackingCode: string;

  constructor(trackingCode: string) {
    this.trackingCode = trackingCode;
  }

  trackEvent(category: string, action: string, label: string, value: number, cb) {
    const data = {
      v: '1',
      tid: this.trackingCode,
      cid: '000',
      t: 'event',
      ec: category,
      ea: action,
      el: label,
      ev: value
    };

    request.post(
      'http://www.google-analytics.com/collect',
      {
        form: data,
      },
      (err, response) => {
        if (err) {
          cb(err);
          return;
        }
        if (response.statusCode !== 200) {
          cb(new Error('Tracking failed'));
          return;
        }
        cb();
      }
    );
  }
}
