export class HttpRequestError extends Error {
  constructor(public status: number, public message: string) {
    super(message);
    Object.setPrototypeOf(this, HttpRequestError.prototype);
  }

  getStatus() {
    return this.status;
  }

  getMessage() {
    return this.message;
  }

  beautify(type?: string) {
    switch (type) {
      case 'string':
        return JSON.stringify({ type: this.constructor.name, message: this.getMessage(), status: this.getStatus() });
      default:
        return { type: this.constructor.name, message: this.getMessage(), status: this.getStatus() };
    }
  }
}
