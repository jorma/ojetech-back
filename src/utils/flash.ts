import { RequestSession } from '../types/RequestSession';
export class Flash {
  private req: RequestSession;

  constructor(req: RequestSession) {
    this.req = req;
  }

  addFlash(error: boolean, message: string): RequestSession {
    this.req.session.flash = {
      error: error,
      message: message
    };
    return this.req;
  }
}
