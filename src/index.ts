import * as express from 'express';
import * as debug from 'debug';
import * as socketio from 'socket.io';
import { Logger, createLogger } from 'bunyan';
import * as Promise from "bluebird";
import Config from './config/config';
import { SequelizeStorageManager } from './storage/storage';
import { SocketsCtrl } from './controllers/SocketsCtrl';
import LlumCronJobs from './crons/Cron';
import { LlumAnalytics } from './utils/llumAnalytics';

import { configureExpress } from './config/express';
import { configureRoutes } from './config/express.routes';
import { configureErrorHandlers } from './config/express.errorHandlers';
import { influx } from './config/express.influx';

let analytics = new LlumAnalytics('UA-83291516-5');

let cache = require('express-redis-cache')({
  expires: 3600
});

export function start(): any {
  analytics.trackEvent('Log', 'Info', 'Start', 1, (err) => {  });

  let storageManager = new SequelizeStorageManager({
    database: Config[process.env.NODE_ENV || 'development'].database,
    username: Config[process.env.NODE_ENV || 'development'].username,
    password: Config[process.env.NODE_ENV || 'development'].password,
    port: Config[process.env.NODE_ENV || 'development'].port
  }, cache);

  return influx.getDatabaseNames().then((names) => {
    if (names.indexOf('express_response_db') === -1) {
      return influx.createDatabase('express_response_db')
    }
  }).then(() => {
    return storageManager.init().then(() => {
      return configureExpress(storageManager, influx).then((app: express.Application) => {
        return configureRoutes(app).then(() => {
          return configureErrorHandlers(app).then(() => {
            return app;
          });
        });
      }).then((app: express.Application) => {
        return new Promise((resolve) => {
          var server = app.listen(1196, () => {
            if(process.env.NODE_ENV === 'production') {
              let crons = new LlumCronJobs(storageManager);
              crons.startCrons();
            }

            let io = socketio(server);
            app.set('sockets', io);

            let socketsController = new SocketsCtrl(storageManager, app);
            resolve(server);
          });
        });
      })
    });
  }).catch((err) => {
    console.error(err);
    console.error(`Error creating Influx database`);
  });
}
