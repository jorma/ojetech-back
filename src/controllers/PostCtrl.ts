import * as express from 'express';
import * as moment from 'moment';
import { SequelizeStorageManager } from '../storage/storage';

import { BaseController } from './BaseCtrl';

export class PostCtrl extends BaseController {
  private storageManager: SequelizeStorageManager;

  constructor(app: express.Application) {
    super(app);
    this.storageManager = app.get('storageManager');

    app.get('/articles/:page?', this.insertLog, this.getArticles);
    app.get('/article/:slug', this.insertLog, this.getArticle);
  }

  insertLog = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    this.storageManager.Log.addLog(req.hostname, req.method, req.url, req.protocol, req.headers['x-forwarded-for'] || req.ip);
    next();
  };

  getArticles = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    let page = req.params.page ? req.params.page : 1;
    const limit = 10;
    return this.storageManager.Post.getPagedPosts(page, limit).then((results) => {
      const posts = results.rows;
      const pages = Math.ceil(results.count / limit);

      return res.render('blog/wall', {
        title: 'Ojetech - Blog',
        posts,
        pages,
        page: parseInt(page)
      });
    });
  };

  getArticle = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    const slug = req.params.slug;

    return this.storageManager.Post.getSinglePost(slug).then((result) => {
      return res.render('blog/singlePost', {
        title: `Ojetech - Blog - ${result.title}`,
        articleTitle: result.title,
        slug: result.slug,
        content: result.content
      });
    })
  };
}
