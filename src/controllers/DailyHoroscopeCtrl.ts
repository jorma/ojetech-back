import { Logger } from 'bunyan';
import * as express from 'express';
import * as request from 'request';
import * as cheerio from 'cheerio';
import * as moment from 'moment';
import { SequelizeStorageManager } from '../storage/storage';

import { BaseController } from './BaseCtrl';

export class DailyHoroscopeCtrl extends BaseController {
  private storageManager: SequelizeStorageManager;
  private cache: any;

  constructor(app: express.Application, cache: any) {
    super(app);
    this.storageManager = app.get('storageManager');
    this.cache = cache;

    /**
     * @api {get} /dailyHoroscope/v001/daily/:zodiac/:date
     * @apiName GetHoroscope
     * @apiGroup DailyHoroscope
     *
     * @apiParam {String} zodiac Sign of horoscope
     * @apiParam {String} date Date of the request in format (DDMMYY)
     *
     * @apiSuccess {String} description The horoscope for this day
     * @apiSuccess {Boolean} cacheable Is cacheable the result?
     *
     * @apiSuccessExample Success-Response:
     *  HTTP/1.1 200 OK
     *  {
     *    "description": "Today will be a great day",
     *    "cacheable": true
     *  }
     */
    app.get('/api/dailyHoroscope/v001/daily/:zodiac/:date', (req, res: any, next) => {
      let date = req.params.date;
      date = moment(`20${date.slice(-2)}-${date.slice(2, 4)}-${date.slice(0, 2)}`).format('YYYY-MM-DD');

      res.express_redis_cache_name = `daily-${req.params.zodiac.toLowerCase()}-${date}`;
      next();
    }, this.cache.route(), this.insertLog, this.getDailyHoroscope);
  }

  insertLog = (req: express.Request, res: express.Response, next: any) => {
    this.storageManager.Log.addLog(req.hostname, req.method, req.url, req.protocol, req.headers['x-forwarded-for'] || req.ip);
    next();
  };

  getDailyHoroscope = (req: express.Request, res: any) => {
    const zodiac = req.params.zodiac,
          date = req.params.date;

    const url: string = `http://www.horoscopodiariodehoy.com/horoscopo-de-hoy-${zodiac}-${date}`;
    let dateDb = moment(`20${date.slice(-2)}-${date.slice(2, 4)}-${date.slice(0, 2)}`).format('YYYY-MM-DD');

    this.storageManager.Horoscope.getHoroscope(zodiac, dateDb).then((horoscope) => {
      if(horoscope) {
        return res.status(200).json({
          description: horoscope.description,
          cacheable: true,
        });
      }

      return request(url, (error, response, html) => {
        if(error) return res.status(500).json(error);
        const $ = cheerio.load(html);
        let text = $('.entry-content>p').text();
        if(text !== '') {
          this.storageManager.Horoscope.addHoroscope(zodiac, dateDb, text);
          return res.status(200).json({
            description: text,
            cacheable: true
          });
        }
        dateDb = moment(`20${date.slice(-2)}-${date.slice(2, 4)}-${date.slice(0, 2)}`).subtract('days', 1).format('YYYY-MM-DD');

        this.storageManager.Horoscope.getHoroscope(zodiac, dateDb).then((horoscope) => {
          if(horoscope) {
            return res.status(200).json({
              description: horoscope.description,
              cacheable: false
            });
          }
          return res.status(200).json({
            description: 'Algo no ha ido bien y hoy es imposible predecir tu día, lo sentimos muchísimo, nuestros videntes están trabajando en ello',
            cacheable: false
          });
        });
      });
    });
  };
}
