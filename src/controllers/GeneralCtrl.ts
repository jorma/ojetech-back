import * as express from 'express';
import * as moment from 'moment';
import { BaseController } from './BaseCtrl';
import { HttpRequestError } from '../utils/llumyErrors';
import { SequelizeStorageManager } from '../storage/storage';
import { RequestSession } from '../types/RequestSession';
import * as os from 'os';
import * as cheerio from 'cheerio';
import * as request from 'request';
import * as Promise from "bluebird";

export class GeneralCtrl extends BaseController {
  private storageManager: SequelizeStorageManager;
  private influx: any;
  private cache: any;

  constructor(app: any, cache: any) {
    super(app);
    this.storageManager = app.get('storageManager');
    this.influx = app.get('influx');
    this.cache = cache;

    app.get('heartbeat', '/heartbeat', this.insertLog, this.getHeartbeat);

    app.get('wall', '/', this.getWall);
    app.get('contact', '/contact', this.insertLog, this.getContact);
    app.get('services', '/services', this.insertLog, this.getServices);

    app.get('admin', '/admin', this.insertLog, this.getAdmin);
    app.get('cabra', '/lacabra', this.insertLog, this.getCabra);
    app.get('robots', '/robots.txt', this.insertLog, this.getRobots);
    app.post('admin.login', '/admin/login', this.insertLog, this.login);
    app.get('admin.dashboard', '/admin/dashboard', this.auth, this.getDashboard);
    app.get('admin.dashboard.horoscope.new', '/admin/dashboard/horoscope/new', this.auth, this.newHoroscopeView);
    app.post('post_admin.dashboard.horoscope.new', '/admin/dashboard/horoscope/new', this.auth, this.newHoroscope);
    app.get('admin.dashboard.horoscope.view', '/admin/dashboard/horoscope/view/:id', this.auth, this.getHoroscope);
    app.post('post_admin.dashboard.horoscope.view', '/admin/dashboard/horoscope/view/:id', this.auth, this.updateHoroscope);
    app.get('admin.dashboard.horoscope.delete', '/admin/dashboard/horoscope/delete/:id', this.auth, this.deleteHoroscope);
    app.get('admin.dashboard.horoscope', '/admin/dashboard/horoscope/:page?', this.auth, this.getDashboardHoroscope);
    app.get('admin.dashboard.horoscope.tomorrow.scrap', '/admin/dashboard/horoscope/tomorrow/scrap', this.auth, this.getHoroscopeTomorrow);

    app.get('admin.dashboard.post', '/admin/dashboard/blog', this.auth, this.getDashboardBlog);
    app.get('admin.dashboard.post.new', '/admin/dashboard/blog/new', this.auth, this.newPostView);
    app.get('*', this.fallback);
  }

  newPostView = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    return res.render('admin/articles/new', {
      title: 'Llumyapps - Admin (Posts) - New',
      user: req.session,
    });
  }

  auth = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    if (req.session && req.session.username === 'root' && req.session.admin) {
      res.locals.path = req.path;
      return next();
    } else {
      res.redirect('/admin');
      return this.influx.writePoints([
        {
          measurement: 'unauthorized_admin',
          tags: { host: os.hostname() },
          fields: { path: req.path, count: 1 },
        }
      ]).catch((error) => {
        console.log('ERROR ' + error.stack);
      });
    }
  };

  getContact = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    return res.render('contact', { title: 'Ojetech - contact', message: 'Hello there!' });
  };

  getServices = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    return res.render('services', { title: 'Ojetech - services', message: 'Hello there!' });
  };

  getHoroscopeTomorrow = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    let promises = [];

    const signs: string[] = [
      'Piscis',
      'Aries',
      'Capricornio',
      'Sagitario',
      'Virgo',
      'Acuario',
      'Escorpio',
      'Leo',
      'Cancer',
      'Libra',
      'Tauro',
      'Geminis'
    ];

    signs.forEach((value) => {
      let url: string = `http://www.elhoroscopodehoy.es/horoscopo-manana/${value.toLowerCase()}.php`;

      promises.push(this.getHoroscopeFromUrl(url, value));
    });

    Promise.all(promises).then(() => res.redirect('/admin/dashboard/horoscope'));
  };

  deleteHoroscope = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    let id = req.params.id;

    this.storageManager.Horoscope.destroyById(id).then((result) => {
      req.flash('info', 'Borrado con éxito');
      return res.redirect('/admin/dashboard/horoscope/1');
    }).catch((error) => {
      return res.json(error);
    });
  };

  newHoroscopeView = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    return res.render('admin/horoscopes/new', {
      title: 'Llumyapps - Admin (Horoscopes) - New',
      user: req.session,
    });
  };

  newHoroscope = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    let { inputZodiac, inputDate, inputDescription } = req.body;

    let dateDb = moment(`20${inputDate.slice(-2)}-${inputDate.slice(2, 4)}-${inputDate.slice(0, 2)}`).format('YYYY-MM-DD');

    return this.storageManager.Horoscope.addHoroscope(inputZodiac, dateDb, inputDescription).then((result) => {
      if (result) {
        return res.redirect('/admin/dashboard/horoscope/view/' + result.id);
      }
    }).catch((error) => {
      return res.json(error);
    });
  };

  updateHoroscope = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    let id = req.params.id;
    let { inputZodiac, inputDate, inputDescription } = req.body;
    let dateDb = moment(`20${inputDate.slice(-2)}-${inputDate.slice(2, 4)}-${inputDate.slice(0, 2)}`).format('YYYY-MM-DD');

    return this.storageManager.Horoscope.getById(id).then((result) => {
      this.cache.del(`daily-${result.zodiac.toLowerCase()}-${moment(result.date).format('YYYY-MM-DD')}`, (error, num) => {
        if (error) {
          console.log(error);
        } else {
          console.log(num);
        }
      });
      result.update({
        zodiac: inputZodiac,
        date: dateDb,
        description: inputDescription,
      }).then((result) => {
        res.redirect('/admin/dashboard/horoscope/view/' + result.id);
      }).catch((error) => {
        res.json(error);
      })
    }).catch((error) => {
      res.json(error);
    });
  };

  getHoroscope = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    let id = req.params.id;

    return this.storageManager.Horoscope.getById(id).then((result) => {
      return res.render('admin/horoscopes/view', {
        title: 'Llumyapps - Admin (Horoscopes) - Detail',
        user: req.session,
        horoscope: result
      });
    }).catch((error) => {
      console.log(error);
    });
  };

  getDashboardHoroscope = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    let page = req.params.page ? req.params.page : 1;

    return this.storageManager.Horoscope.getPagedHoroscopes(page).then((results) => {
      const horoscopes = results.rows;
      const pages = Math.ceil(results.count / 24.0);

      return res.render('admin/horoscopes', {
        title: 'Llumyapps - Admin (Horoscopes)',
        flash: {
          info: req.flash('info'),
          error: req.flash('error'),
        },
        horoscopes,
        pages,
        page: parseInt(page),
      });
    });
  };

  getDashboardBlog = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    let page = req.params.page ? req.params.page : 1;
    const limit = 24;

    return this.storageManager.Post.getPagedPosts(page, limit).then((results) => {
      const posts = results.rows;
      const pages = Math.ceil(results.count / 24.0);

      return res.render('admin/articles', {
        title: 'Llumyapps - Admin (Posts)',
        flash: {
          info: req.flash('info'),
          error: req.flash('error')
        },
        posts,
        pages,
        page: parseInt(page)
      });
    });
  }

  getDashboard = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    return res.redirect('/admin/dashboard/horoscope');
  };

  login = (req: RequestSession, res: express.Response, next: express.NextFunction) => {
    if (req.body.username &&
        req.body.username === 'root' &&
        req.body.password &&
        req.body.password === 's3dT35t1605') {
      req.session.username = 'root';
      req.session.admin = true;

      return res.redirect('/admin/dashboard');
    } else {
      req.session.username = undefined;
      req.session.admin = false;

      return res.redirect('/admin');
    }
  };

  insertLog = (req: any, res: express.Response, next: any) => {
    if (process.env.NODE_ENV !== 'all') {
      req.producerService.publish('log', {
        hostname: req.hostname,
        method: req.method,
        url: req.url,
        protocol: req.protocol
      });
    }

    this.storageManager.Log.addLog(req.hostname, req.method, req.url, req.protocol, req.headers['x-forwarded-for'] || req.ip);
    next();
  };

  getHeartbeat = (req: express.Request, res: express.Response) => {
    return res.status(200).json({ description: 'Heartbeat ok' });
  };

  getWall = (req: express.Request, res: express.Response) => {
    return res.render('wall', { title: 'Llumyapps', message: 'Hello there!' });
  };

  getRoot = (req: express.Request, res: express.Response) => {
    return res.render('index', { title: 'Llumyapps', message: 'Hello there!' });
  };

  getCabra = (req: express.Request, res: express.Response) => {
    return res.render('lacabra', { title: 'Beroomers' });
  };

  getRobots = (req: express.Request, res: express.Response) => {
    res.type('text/plain');
    return res.send('User-agent: *\nDisallow: /');
  };

  getAdmin = (req: any, res: express.Response) => {
    return res.render('admin/login', { title: 'Llumyapps - Admin' });
  };

  fallback = (req: express.Request, res: express.Response) => {
    throw new HttpRequestError(404, 'Not found');
  };

  getHoroscopeFromUrl(url, value): Promise<any> {
    return new Promise((resolve) => {
      request(url, (error, response, html) => {
        if (!error) {
          const $ = cheerio.load(html);
          let tomorrowScrapped = $('.font12.gray>span').text().replace('Horóscopo del día', '').trim();
          let tomorrow = moment().add(1, 'days').format('DD/MM/YYYY');
          if (tomorrowScrapped === tomorrow) {
            let description = $('span.prediccion').text();
            tomorrow = tomorrow.split('/')[1]+'/'+tomorrow.split('/')[0]+'/'+tomorrow.split('/')[2];
            let dateDb = moment(tomorrow).format('YYYY-MM-DD');
            this.storageManager.Horoscope.getHoroscope(value, dateDb).then((result) => {
              if (!result) {
                this.storageManager.Horoscope.addHoroscope(value, dateDb, description).then(resolve);
              } else {
                resolve();
              }
            });
          } else {
            resolve();
          }
        }
      });
    });
  }
}
