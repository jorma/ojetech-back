import * as express from 'express';
import { BaseController } from './BaseCtrl';
import { StorageManager } from '../storage/storageManager.interface';

export class SocketsCtrl extends BaseController {
  private storageManager: StorageManager;

  constructor(storageManager: StorageManager, app: express.Application) {
    super(app);
    this.storageManager = storageManager;
    let io = app.get('sockets');
    io.on('connection', (socket) => {
      socket.on('disconnect', () => {
        console.log('User disconnected');
      });

      socket.on('add_message', (message) => {
        io.emit('message', { type: 'new-message', text: message });
      });

      socket.on('get_requests', () => {
        io.emit('requests', { type: 'number_request', number: 2 });
      })
    });
  }
}
