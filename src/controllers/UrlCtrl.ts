import { BaseController } from "./BaseCtrl";
import { SequelizeStorageManager } from "../storage/storage";
import { Logger } from "bunyan";
import * as express from 'express';
import { Base58 } from '../utils/base58';
import Config from '../config/config';

export class UrlCtrl extends BaseController {
  private storageManager: SequelizeStorageManager;
  private base58: Base58;

  constructor(app: express.Application) {
    super(app);
    this.storageManager = app.get('storageManager');
    this.base58 = new Base58();

    app.post('/api/shorten/v001/short', this.insertLog, this.shortUrl);
    app.get('/l/:encoded', this.insertLog, this.decodeUrl);
  }

  insertLog = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    this.storageManager.Log.addLog(req.hostname, req.method, req.url, req.protocol, req.headers['x-forwarded-for'] || req.ip);
    return next();
  };

  shortUrl = (req: express.Request, res: express.Response) => {
    let longUrl = req.body.url;
    let shortUrl = '';

    if (!longUrl)
      return res.status(404).json({ description: 'url cannot be null' });

    if (longUrl.indexOf('http') === -1)
      longUrl = `http://${longUrl}`;

    return this.storageManager.Url.findByLongUrl(longUrl).then((row) => {
      if (!row) {
        return this.storageManager.Url.insertUrl(longUrl).then((row) => {
          shortUrl = this.base58.encode(row.id);
          return res.status(200).json({ shortUrl: Config[process.env.NODE_ENV || 'development'].domain + '/l/' +shortUrl });
        });
      }

      return res.status(200).json({ shortUrl: Config[process.env.NODE_ENV || 'development'].domain + '/l/' + this.base58.encode(row.id) });
    });
  };

  decodeUrl = (req: express.Request, res: express.Response) => {
    if (!req.params.encoded)
      return res.redirect(Config[process.env.NODE_ENV || 'development'].domain);

    let base58Id = req.params.encoded;

    let id = this.base58.decode(base58Id);

    this.storageManager.Url.findById(id).then((row) => {
      if (!row) {
        return res.redirect(Config[process.env.NODE_ENV || 'development'].domain);
      }

      // Añadir click a la URL
      return res.redirect(row.longUrl);
    });
  };
}
