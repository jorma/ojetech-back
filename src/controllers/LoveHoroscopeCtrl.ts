import { Logger } from 'bunyan';
import * as express from 'express';
import * as request from 'request';
import * as cheerio from 'cheerio';
import * as moment from 'moment';
import { SequelizeStorageManager } from '../storage/storage';
import { BaseController } from './BaseCtrl';

export class LoveHoroscopeCtrl extends BaseController {
  private storageManager: SequelizeStorageManager;

  constructor(app: express.Application) {
    super(app);
    this.storageManager = app.get('storageManager');
    /**
     * @api {get} /loveHoroscope/v001/daily/:zodiac/:date
     * @apiName GetLoveHoroscope
     * @apiGroup LoveDailyHoroscope
     *
     * @apiParam {String} zodiac Sign of horoscope
     * @apiParam {String} date Date of the request in format (DDMMYY)
     *
     * @apiSuccess {String} description The horoscope for this day
     * @apiSuccess {Boolean} cacheable Is cacheable the result?
     *
     * @apiSuccessExample Success-Response:
     *  HTTP/1.1 200 OK
     *  {
     *    "description": "Today will be a great day and you will fall in love",
     *    "cacheable": true
     *  }
     */
    // TODO: Now we will accept an optional parameter language
    app.get('/api/loveHoroscope/v001/daily/:zodiac/:date', this.insertLog, this.getDailyLoveHoroscope);
  }

  insertLog = (req: express.Request, res: express.Response, next: any) => {
    this.storageManager.Log.addLog(req.hostname, req.method, req.url, req.protocol, req.headers['x-forwarded-for'] || req.ip);
    next();
  };

  getDailyLoveHoroscope = (req: express.Request, res: express.Response) => {
    const zodiac = req.params.zodiac,
          date = req.params.date;

    const url: string = `http://www.horoscopo.com/horoscopo-diario-amor-gratis-${zodiac}.html`;
    let dateDb = moment(`20${date.slice(-2)}-${date.slice(2, 4)}-${date.slice(0, 2)}`).format('YYYY-MM-DD');

    this.storageManager.LoveHoroscope.getLoveHoroscope(zodiac, dateDb).then((horoscope) => {
      if(horoscope) {
        return res.status(200).json({
          description: horoscope.description,
          cacheable: true,
        });
      }

      return request(url, (error, response, html) => {
        if(error) return res.status(500).json(error);
        const $ = cheerio.load(html);
        let dateInHTML = $('div.advert').first().text().replace(/.*, /g, '').toLowerCase();
        let dateRequested = moment(`20${date.slice(-2)}-${date.slice(2, 4)}-${date.slice(0, 2)}`).locale('es').format('LL');

        if (dateRequested !== dateInHTML)
          return res.status(404).json({
            description: 'Estamos calculando tu predicción Astral en estos momentos. Intentalo de nuevo más tarde.',
            cacheable: false,
          });

        let textLine = $('#textline').first().text();
        if(textLine === '')
          return res.status(404).json({
            description: 'Ups, algo ha ocurrido y se ha transpapelado tu predicción, intentalo de nuevo más tarde.',
            cacheable: false,
          });

        this.storageManager.LoveHoroscope.addLoveHoroscope(zodiac, dateDb, textLine);
        return res.status(200).json({
          description: textLine,
          cacheable: true
        })
      });
    });
  }
}
