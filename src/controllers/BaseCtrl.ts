import { Logger } from 'bunyan';
import { inject } from 'awilix-express';
import { dependencies } from '../config/dependencies';

export abstract class BaseController {
  constructor(app: any) {
    dependencies.forEach(element => {
      app.use(inject(element.name));
    });
  }
}
