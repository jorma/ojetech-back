import { Logger, createLogger } from 'bunyan';

export class LogService {
  private log: any;

  constructor() {
    this.log = createLogger({
      name: 'internal'
    });
  }

  public getLog() {
    return this.log;
  }
}
