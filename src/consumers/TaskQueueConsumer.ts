import * as amqp from 'amqplib';

amqp.connect('amqp://ratt:11umyapp5@localhost').then(conn => {
  process.once('SIGINT', () => {
    conn.close();
  });

  return conn.createChannel().then(ch => {
    let ok = ch.assertQueue('task_queue', { durable: true });
    ok = ok.then(() => { ch.prefetch(1); });
    ok = ok.then(() => {
      ch.consume('task_queue', doWork, { noAck: false });
      console.log('TaskQueueConsumer [UP]');
    });

    return ok;

    function doWork(msg) {
      let body = msg.content.toString();
      console.log(`[x] Received '${body}'`);
      let secs = body.split('.').length - 1;
      setTimeout(() => {
        console.log('[x] Done');
        ch.ack(msg);
      }, secs * 1000);
    }
  });
}).catch(console.warn);
