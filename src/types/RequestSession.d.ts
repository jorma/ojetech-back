import { Request } from 'express';
export interface RequestSession extends Request {
  session: {
    username: string,
    admin: boolean,
    flash?: Object,
  }
}
