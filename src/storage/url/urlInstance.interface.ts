import * as Sequelize from 'sequelize';
import { UrlAttribute } from './urlAttribute.interface';

export interface UrlInstance extends Sequelize.Instance<UrlAttribute>, UrlAttribute {

}
