export interface UrlAttribute {
  id?: string,
  longUrl?: string,
  clicks?: number,
}
