import * as Sequelize from 'sequelize';
import * as Promise from 'bluebird';
import { UrlInstance } from './urlInstance.interface';
import { UrlAttribute } from './urlAttribute.interface';
import { UrlModel } from './urlModel.interface';

export class UrlManager implements UrlModel {
  sequelize: Sequelize.Sequelize;
  Url: Sequelize.Model<UrlInstance, UrlAttribute>;

  constructor(sequelize: Sequelize.Sequelize) {
    this.sequelize = sequelize;
    this.Url = this.sequelize.define<UrlInstance, UrlAttribute>('Url', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      longUrl: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      clicks: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      }
    }, {
      tableName: 'url',
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    });
  }

  findByLongUrl(longUrl: string): Promise<any> {
    return this.Url.find({ where: { longUrl: longUrl } });
  }

  insertUrl(longUrl: string): Promise<any> {
    return this.sequelize.transaction((transaction: Sequelize.Transaction) => {
      return this.Url.create({ longUrl: longUrl }, { transaction: transaction })
    });
  }

  findById(id: number): Promise<any> {
    return this.Url.find({ where: { id: id } });
  }
}
