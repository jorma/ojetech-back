export interface HoroscopeAttribute {
  id?: string;
  date?: string;
  zodiac?: string;
  description?: string;
}
