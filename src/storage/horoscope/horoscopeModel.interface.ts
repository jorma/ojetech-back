import * as Promise from 'bluebird';

export interface HoroscopeModel {
  getHoroscope(zodiac: string, date: string): Promise<any>;
  getAllHoroscopes(): Promise<any>;
  addHoroscope(zodiac: string, date: string, description: string): Promise<any>;
  getPagedHoroscopes(offset: number): Promise<any>;
}
