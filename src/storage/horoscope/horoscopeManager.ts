import * as Sequelize from 'sequelize';
import * as Promise from 'bluebird';
import * as uuid from 'node-uuid';
import { HoroscopeInstance } from './horoscopeInstance.interface';
import { HoroscopeAttribute } from './horoscopeAttribute.interface';
import { HoroscopeModel } from './horoscopeModel.interface';
import * as moment from 'moment';

export class HoroscopeManager implements HoroscopeModel {
  sequelize: Sequelize.Sequelize;
  Horoscope: Sequelize.Model<HoroscopeInstance, HoroscopeAttribute>;
  cache: any;
  constructor(sequelize: Sequelize.Sequelize, cache: any) {
    this.sequelize = sequelize;
    this.cache = cache;
    this.Horoscope = this.sequelize.define<HoroscopeInstance, HoroscopeAttribute>('Horoscope', {
      id: {
        type: Sequelize.STRING(128),
        allowNull: false,
        primaryKey: true
      },
      zodiac: {
        type: Sequelize.STRING(128),
        allowNull: false,
      },
      date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT(),
        allowNull: false
      }
    }, {
      tableName: 'horoscope',
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    });
  }

  destroyById(id: string): Promise<any> {
    return this.Horoscope.find({ where: { id: id } }).then((horoscope) => {
      let dateDb = moment(horoscope.date).format('YYYY-MM-DD');

      this.cache.del(`daily-${horoscope.zodiac.toLowerCase()}-${dateDb}`, (err, num) => {

      });
      horoscope.destroy().thenReturn();
    }).catch((error) => {

    });
  }

  getHoroscope(zodiac: string, date: string): Promise<any> {
    return this.Horoscope.find({ where: { zodiac: zodiac, date: date } });
  }

  getById(id: string): Promise<any> {
    return this.Horoscope.findOne({
      where: {
        id: id
      }
    });
  }

  addHoroscope(zodiac: string, date: string, description: string): Promise<any> {
    return this.sequelize.transaction((transaction: Sequelize.Transaction) => {
      let insertId = uuid.v4();
      return this.Horoscope.create({
        id: insertId,
        zodiac: zodiac,
        date: date,
        description: description
      }, { transaction: transaction });
    });
  }

  getAllHoroscopes(): Promise<any> {
    return this.Horoscope.findAll();
  }

  getPagedHoroscopes(offset: number): Promise<any> {
    const limit = 24;
    return this.Horoscope.findAndCountAll({
      limit: limit,
      offset: (offset - 1) * limit,
      order: [
        [ 'date', 'DESC' ]
      ]
    });
  }
}
