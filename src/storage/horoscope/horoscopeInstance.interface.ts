import * as Sequelize from 'sequelize';
import { HoroscopeAttribute } from './horoscopeAttribute.interface';

export interface HoroscopeInstance extends Sequelize.Instance<HoroscopeAttribute>, HoroscopeAttribute {

}
