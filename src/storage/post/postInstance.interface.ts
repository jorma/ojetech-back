import * as Sequelize from 'sequelize';
import { PostAttribute } from './postAttribute.interface';

export interface PostInstance extends Sequelize.Instance<PostAttribute>, PostAttribute {

}
