export interface PostAttribute {
  id?: string;
  title?: string;
  mainPicture?: string;
  content?: string;
  slug?: string;
}
