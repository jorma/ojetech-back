import * as Sequelize from 'sequelize';
import * as Promise from 'bluebird';
import * as uuid from 'node-uuid';
import { PostInstance } from './postInstance.interface';
import { PostModel } from "./postModel.interface";
import { PostAttribute } from "./postAttribute.interface";

export class PostManager implements PostModel {
  sequelize: Sequelize.Sequelize;
  Post: Sequelize.Model<PostInstance, PostAttribute>;

  constructor(sequelize: Sequelize.Sequelize) {
    this.sequelize = sequelize;

    this.Post = this.sequelize.define<PostInstance, PostAttribute>('Post', {
      id: {
        type: Sequelize.STRING(128),
        allowNull: false,
        primaryKey: true
      },
      title: {
        type: Sequelize.STRING(256),
        allowNull: false,
      },
      mainPicture: {
        type: Sequelize.STRING(256),
        allowNull: false,
        defaultValue: ''
      },
      content: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      slug: {
        type: Sequelize.STRING(128),
        allowNull: false
      }
    }, {
      tableName: 'post',
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    });
  }

  getPagedPosts(offset: number, limit: number): Promise<any> {
    return this.Post.findAndCountAll({
      limit: limit,
      offset: (offset - 1) * limit,
      order: [
        [ 'date', 'DESC' ]
      ]
    });
  }

  addPost(title: string, mainPicture: string, content: string, slug: string): Promise<any> {
    return this.sequelize.transaction((transaction: Sequelize.Transaction) => {
      let insertId = uuid.v4();

      return this.Post.create({
        id: insertId,
        title: title,
        mainPicture: mainPicture,
        content: content,
        slug: slug
      }, { transaction: transaction });
    });
  }

  getSinglePost(slug: string): Promise<any> {
    return this.Post.findOne({
      where: {
        slug: slug
      }
    });
  }
}
