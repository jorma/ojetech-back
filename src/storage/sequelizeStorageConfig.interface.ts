export interface SequelizeStorageConfig {
  database: string,
  username: string,
  password: string,
  port: number;
}
