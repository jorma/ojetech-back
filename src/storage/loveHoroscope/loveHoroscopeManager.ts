import * as Sequelize from 'sequelize';
import * as Promise from 'bluebird';
import * as uuid from 'node-uuid';
import { LoveHoroscopeInstance } from './loveHoroscopeInstance.interface';
import { LoveHoroscopeAttribute } from './loveHoroscopeAttribute.interface';
import { LoveHoroscopeModel } from './loveHoroscopeModel.interface';

export class LoveHoroscopeManager implements LoveHoroscopeModel {
  sequelize: Sequelize.Sequelize;
  LoveHoroscope: Sequelize.Model<LoveHoroscopeInstance, LoveHoroscopeAttribute>;
  constructor(sequelize: Sequelize.Sequelize) {
    this.sequelize = sequelize;
    this.LoveHoroscope = this.sequelize.define<LoveHoroscopeInstance, LoveHoroscopeAttribute>('LoveHoroscope', {
      id: {
        type: Sequelize.STRING(128),
        allowNull: false,
        primaryKey: true
      },
      zodiac: {
        type: Sequelize.STRING(128),
        allowNull: false
      },
      date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      description: {
        type: Sequelize.TEXT(),
        allowNull: false
      }
    }, {
      tableName: 'lovehoroscope',
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    });
  }

  getLoveHoroscope(zodiac: string, date: string): Promise<any> {
    return this.LoveHoroscope.find({ where: { zodiac: zodiac, date: date } });
  }

  addLoveHoroscope(zodiac: string, date: string, description: string): Promise<any> {
    return this.sequelize.transaction((transaction: Sequelize.Transaction) => {
      let insertId = uuid.v4();
      return this.LoveHoroscope.create({
        id: insertId,
        zodiac: zodiac,
        date: date,
        description: description
      }, { transaction: transaction });
    });
  }
}
