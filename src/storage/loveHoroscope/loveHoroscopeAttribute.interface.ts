export interface LoveHoroscopeAttribute {
  id?: string;
  date?: string;
  zodiac?: string;
  description?: string;
}
