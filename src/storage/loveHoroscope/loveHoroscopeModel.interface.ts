import * as Sequelize from 'sequelize';
import * as Promise from 'bluebird';

export interface LoveHoroscopeModel {
  getLoveHoroscope(zodiac: string, date: string): Promise<any>;
  addLoveHoroscope(zodiac: string, date: string, description: string): Promise<any>;
}
