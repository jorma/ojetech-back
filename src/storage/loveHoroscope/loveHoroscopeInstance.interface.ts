import * as Sequelize from 'sequelize';
import { LoveHoroscopeAttribute } from './loveHoroscopeAttribute.interface';

export interface LoveHoroscopeInstance extends Sequelize.Instance<LoveHoroscopeAttribute>, LoveHoroscopeAttribute {

}
