import * as Promise from 'bluebird';

export interface StorageManager {
  init(force?: boolean): Promise<any>;
}
