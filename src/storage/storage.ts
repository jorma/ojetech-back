import * as Promise from 'bluebird';
import * as Sequelize from 'sequelize';
import { Logger } from 'bunyan';
import * as uuid from 'node-uuid';
import { hashSync, genSaltSync, compareSync } from 'bcryptjs';

import { SequelizeStorageConfig } from './sequelizeStorageConfig.interface';
import { StorageManager } from './storageManager.interface';
import { HoroscopeManager } from './horoscope/horoscopeManager';
import { LoveHoroscopeManager } from './loveHoroscope/loveHoroscopeManager';
import { LogManager } from './log/logManager';
import { UrlManager } from "./url/urlManager";
import { PostManager } from "./post/postManager";

export class SequelizeStorageManager implements StorageManager {
  public sequelize: Sequelize.Sequelize;

  public Horoscope: HoroscopeManager;
  public LoveHoroscope: LoveHoroscopeManager;
  public Log: LogManager;
  public Url: UrlManager;
  public Post: PostManager;

  private config: SequelizeStorageConfig;
  private cache: any;

  constructor(config: SequelizeStorageConfig, cache: any) {
    this.config = config;
    this.cache = cache;

    this.sequelize = new Sequelize(this.config.database, this.config.username, this.config.password, { dialect: 'mysql', port: this.config.port, host: 'localhost' });

    this.Horoscope = new HoroscopeManager(this.sequelize, this.cache);
    this.LoveHoroscope = new LoveHoroscopeManager(this.sequelize);
    this.Log = new LogManager(this.sequelize);
    this.Url = new UrlManager(this.sequelize);
    this.Post = new PostManager(this.sequelize);
  }

  init(force?: boolean): Promise<any> {
    force = force || false;

    if (process.env.NODE_ENV === 'production') {
      return this.sequelize.sync({
        force: force,
        logging: () => false,
      });
    } else {
      return this.sequelize.sync({
        force: force,
      });
    }
  }
}
