export interface LogAttribute {
  id?: string,
  hostname?: string,
  method?: string,
  url?: string,
  protocol?: string,
  ip?: string
}
