import * as Sequelize from 'sequelize';
import * as Promise from 'bluebird';
import * as uuid from 'node-uuid';
import { LogInstance } from './logInstance.interface';
import { LogAttribute } from './logAttribute.interface';
import { LogModel } from './logModel.interface';

export class LogManager implements LogModel {
  sequelize: Sequelize.Sequelize;
  Log: Sequelize.Model<LogInstance, LogAttribute>;
  constructor(sequelize: Sequelize.Sequelize) {
    this.sequelize = sequelize;
    this.Log = this.sequelize.define<LogInstance, LogAttribute>('Log', {
      id: {
        type: Sequelize.STRING(128),
        allowNull: false,
        primaryKey: true
      },
      hostname: {
        type: Sequelize.STRING(128),
        allowNull: false
      },
      method: {
        type: Sequelize.STRING(16),
        allowNull: false
      },
      url: {
        type: Sequelize.STRING(128),
        allowNull: false
      },
      protocol: {
        type: Sequelize.STRING(32),
        allowNull: false
      },
      ip: {
        type: Sequelize.STRING(64),
        allowNull: false
      }
    }, {
      tableName: 'log',
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    });
  }

  addLog(hostname: string, method: string, url: string, protocol: string, ip: string): Promise<any> {
    return this.sequelize.transaction((transaction: Sequelize.Transaction) => {
      let insertId = uuid.v4();
      return this.Log.create({
        id: insertId,
        hostname: hostname,
        method: method,
        url: url,
        protocol: protocol,
        ip: ip
      }, { transaction: transaction })
    });
  }
}
