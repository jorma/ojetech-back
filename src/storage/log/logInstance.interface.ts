import * as Sequelize from 'sequelize';
import { LogAttribute } from './logAttribute.interface';

export interface LogInstance extends Sequelize.Instance<LogAttribute>, LogAttribute {
  addLog(hostname: string, method: string, url: string, protocol: string, ip: string): Promise<any>;
}
