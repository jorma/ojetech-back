import * as Sequelize from 'sequelize';
import * as Promise from 'bluebird';

export interface LogModel {
  addLog(hostname: string, method: string, url: string, protocol: string, ip: string): Promise<any>;
}
