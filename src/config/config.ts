let config = require('./configVars.json');
class Config {
  public development = {
    username: config.development.username,
    password: config.development.password,
    database: config.development.database,
    host: config.development.host,
    dialect: config.development.dialect,
    port: config.development.port,
    jwt: {
      secret: config.development.jwt.secret
    },
    domain: config.development.domain
  };
  public production = {
    username: config.production.username,
    password: config.production.password,
    database: config.production.database,
    host: config.production.host,
    port: config.production.port,
    jwt: {
      secret: config.production.jwt.secret
    },
    domain: config.production.domain
  };
  public vagrant = {
    username: config.vagrant.username,
    password: config.vagrant.password,
    database: config.vagrant.database,
    host: config.vagrant.host,
    port: config.vagrant.port,
    jwt: {
      secret: config.vagrant.jwt.secret
    },
    domain: config.vagrant.domain
  };

  public test = {
    username: config.test.username,
    password: config.test.password,
    database: config.test.database,
    host: config.test.host,
    port: config.test.port,
    jwt: {
      secret: config.test.jwt.secret
    }
  };

  constructor() {
  }
}

export default new Config();
