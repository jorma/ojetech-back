import * as express from 'express';
import * as path from 'path';
import { Logger } from 'bunyan';
import * as Promise from "bluebird";
import { DailyHoroscopeCtrl } from '../controllers/DailyHoroscopeCtrl';
import { LoveHoroscopeCtrl } from '../controllers/LoveHoroscopeCtrl';
import { UrlCtrl } from '../controllers/UrlCtrl';
import { GeneralCtrl } from '../controllers/GeneralCtrl';
import { PostCtrl } from "../controllers/PostCtrl";

let cache = require('express-redis-cache')({
  expires: 3600
});

export function configureRoutes(app: express.Application) {
  return new Promise((resolve) => {
    new DailyHoroscopeCtrl(app, cache);
    new LoveHoroscopeCtrl(app);
    new UrlCtrl(app);
    new PostCtrl(app);

    app.use('/assets', express.static(path.join(__dirname, '../../assets')));
    app.use('/resources', express.static(path.join(__dirname, '../../public')));
    app.use('/back-office', express.static(path.join(__dirname, '../../public')));
    app.use('/apidoc', express.static(path.join(__dirname, '../../public/apidoc')));

    new GeneralCtrl(app, cache);

    resolve();
  });
}
