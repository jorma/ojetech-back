import * as express from 'express';
import * as Promise from "bluebird";
import * as bodyParser from 'body-parser';
import * as favicon from 'serve-favicon';
import * as Raven from 'raven';
import * as cookieParser from 'cookie-parser';
import * as compression from 'compression';
import * as helmet from 'helmet';
import * as cors from 'cors';
import * as os from 'os';
import * as session from 'express-session';
import * as redis from 'connect-redis';
import { createContainer, Lifetime } from 'awilix';
import { scopePerRequest } from 'awilix-express';
import { dependencies } from './dependencies';
import { StorageManager } from '../storage/storageManager.interface';

if (process.env.NODE_ENV !== 'all') {
  require('../consumers/TaskQueueConsumer');
  require('../consumers/LogQueueConsumer');
}

let flash = require('connect-flash');
let RedisStore = redis(session);

export function configureExpress(storageManager: StorageManager, influx: any): Promise<any> {
  return Promise.resolve(express()).then((app) => {
    const container = createContainer();
    dependencies.forEach(element => {
      if (element.type === 'class') {
        container.registerClass({
          [element.name]: [element.service, element.lifetime]
        });
      }
    });

    app.use(scopePerRequest(container));

    if (process.env.NODE_ENV === 'production') {
      const opbeat = require('opbeat').start({
        appId: 'b45b76b426',
        organizationId: 'dc723538ad3d4c6fb2f1ead2306a1c95',
        secretToken: 'c80948aca0ff041c35a25b23d661ae8fedec1a8c'
      });

      app.use(opbeat.middleware.express());
    }

    Raven.config('https://aabbf454c9a44037bafe307ab9d305f4:4e5de96eb87249fca394591742266f5a@sentry.io/144929').install();
    app.use(Raven.requestHandler());

    app.set('etag', false);
    app.set('trust proxy', true);
    app.set('view engine', 'pug');

    require('express-reverse')(app);


    if (process.env.NODE_ENV !== 'production') {
      app.set('view options', {
        debug: true
      });
    }

    app.set('views', __dirname + '/../views');
    app.disable('x-powered-by');

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(flash());
    app.use(cookieParser());

    app.use(compression());
    app.use(helmet());

    app.use(cors({
      origin: true,
      methods: ['GET', 'POST', 'PUT', 'DELETE'],
      allowedHeaders: ['Content-Type', 'Authorization']
    }));

    app.use(session({
      store: new RedisStore({
        host: 'localhost',
        port: 6379,
        logErrors: true
      }),
      secret: 'Kores_2016',
      resave: true,
      saveUninitialized: true,
      cookie: { secure: false }
    }));

    if (process.env.NODE_ENV !== 'production') {
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        console.log({ method: req.method, url: req.url, protocol: req.protocol, ip: req.ip, hostname: req.hostname }, `${req.method} ${req.path}`);
        return next();
      });
    }

    app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
      const start = Date.now();
      res.on('finish', () => {
        const duration = Date.now() - start;
        console.log(`Request to ${req.path} took ${duration}ms`);
        influx.writePoints([
          {
            measurement: 'response_times',
            tags: { host: os.hostname() },
            fields: { duration, path: req.path },
          }
        ]).catch(err => {
          console.error(`Error saving data to InfluxDB! ${err.stack}`);
        })
      });

      return next();
    });

    app.set('storageManager', storageManager);
    app.set('influx', influx);

    return app;
  });
}
