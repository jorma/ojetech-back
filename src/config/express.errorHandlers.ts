import * as express from 'express';
import * as Promise from "bluebird";
import * as Raven from 'raven';
import { HttpRequestError } from '../utils/llumyErrors';
import { LlumAnalytics } from '../utils/llumAnalytics';

let analytics = new LlumAnalytics('UA-83291516-5');

export function configureErrorHandlers(app:express.Application) {
  return new Promise((resolve) => {
    app.use(function (err: Error, req: express.Request, res: any, next: express.NextFunction) {
      if (err) {
        if (err.name === 'UnauthorizedError') {
          res.status(401).json({
            code: 401,
            description: 'Invalid access token'
          });
        } else if (err instanceof HttpRequestError) {
          if (process.env.NODE_ENV === 'production') {
            analytics.trackEvent('Log', 'Error', 'HttpRequestError', 1, (err) => {  });
            Raven.captureException(err.beautify('string'), { req: req });
          }
          res.status(err.getStatus()).json(err.beautify());
        } else {
          if (process.env.NODE_ENV !== 'production') {
            res.status(500).json({ error: err.stack });
          } else {
            res.status(500).render('error', { title: 'Llumyapps - Ups!' });
          }
        }
      }

      next();
    });

    resolve();
  });
}
