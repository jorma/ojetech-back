import { LogService } from '../services/LogService';
import { Lifetime } from 'awilix';
import { ProducerService } from '../services/ProducerService';

export interface dependency {
  name;
  service;
  type;
  lifetime;
}

let dependencies: Array<dependency>;

if (process.env.NODE_ENV !== 'all') {
  dependencies = [
    {
      name: 'logService',
      service: LogService,
      type: 'class',
      lifetime: Lifetime.SCOPED
    },
    {
      name: 'producerService',
      service: ProducerService,
      type: 'class',
      lifetime: Lifetime.SCOPED
    }
  ];
} else {
  dependencies = [
    {
      name: 'logService',
      service: LogService,
      type: 'class',
      lifetime: Lifetime.SCOPED
    }
  ];
}

export {
  dependencies
};
