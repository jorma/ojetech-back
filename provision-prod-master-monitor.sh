#!/usr/bin/env bash
ansible-playbook --ask-pass -i hosts.ini -l production -e "mode=master" ./provisioning/provision-monitor.yml -vvv
